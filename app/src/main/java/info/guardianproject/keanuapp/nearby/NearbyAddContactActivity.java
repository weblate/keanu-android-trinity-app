package info.guardianproject.keanuapp.nearby;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

import info.guardianproject.keanu.core.model.Contact;
import info.guardianproject.keanu.core.ImApp;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanu.core.ui.friends.FriendListItem;
import info.guardianproject.keanu.core.ui.friends.FriendViewHolder;

import static android.content.Intent.EXTRA_TEXT;

public class NearbyAddContactActivity extends AppCompatActivity {

  //  private MessageListener mMessageListener;
 //   private Message mMessage;
    private static ImApp mApp;
    private static RecyclerView mList;
    private static HashMap<String,Contact> contactList = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby);

        setTitle("");

        ImageView iv = (ImageView) findViewById(R.id.nearbyIcon);
        mList = findViewById(R.id.nearbyList);
        mList.setLayoutManager(new LinearLayoutManager(this));
        mList.setItemAnimator(new DefaultItemAnimator());

        NearbyListRecyclerViewAdapter adapter = new NearbyListRecyclerViewAdapter(this,new ArrayList<Contact>(contactList.values()));
        mList.setAdapter(adapter);

        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                iv,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.2f));
        scaleDown.setDuration(310);
        scaleDown.setInterpolator(new FastOutSlowInInterpolator());

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();

        mApp = (ImApp)getApplication();

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Showing status
        if(checkPlayServices())
        {
            String nearbyMessage = getIntent().getStringExtra(EXTRA_TEXT);
            initNearby(nearbyMessage);
        }
        else
        {
            Toast.makeText(this, R.string.nearby_not_supported,Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 2404;

    private boolean checkPlayServices() {

        return true;
    }

    private void initNearby (String nearbyMessage) {

        /**
        mMessageListener = new MessageListener() {
            @Override
            public void onFound(Message message) {
              //  Log.d(TAG, "Found message: " + new String(message.getContent()));

                OnboardingManager.DecodedInviteLink diLink = OnboardingManager.decodeInviteLink(new String(message.getContent()));

                if (diLink != null) {
                        addContact(diLink);
                }
            }

            @Override
            public void onLost(Message message) {
                Log.d(LOG_TAG, "Lost sight of message: " + new String(message.getContent()));
            }
        };

        mMessage = new Message(nearbyMessage.getBytes());**/
    }


    private static void ignoreContact (Contact contact)
    {
        contactList.remove(contact);
        refreshList();
    }


    private static void refreshList ()
    {
        NearbyListRecyclerViewAdapter adapter = new NearbyListRecyclerViewAdapter(mApp,new ArrayList<Contact>(contactList.values()));
        mList.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();

    //    Nearby.getMessagesClient(this).publish(mMessage);
     //   Nearby.getMessagesClient(this).subscribe(mMessageListener);
    }

    @Override
    public void onStop() {
      //  Nearby.getMessagesClient(this).unpublish(mMessage);
      //  Nearby.getMessagesClient(this).unsubscribe(mMessageListener);

        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    public static class NearbyListRecyclerViewAdapter
            extends RecyclerView.Adapter<FriendViewHolder> {

        private final TypedValue mTypedValue = new TypedValue();
        private int mBackground;
        private Context mContext;
        private  ArrayList<Contact> mItemList;

        public NearbyListRecyclerViewAdapter(Context context,  ArrayList<Contact> itemList) {
            super();

            mItemList = itemList;
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
            mBackground = mTypedValue.resourceId;
            mContext = context;

        }


        @Override
        public FriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


            FriendListItem view = (FriendListItem) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.friend_view, parent, false);
            view.setShowPresence(false);
            view.setBackgroundResource(mBackground);

            FriendViewHolder holder = view.getViewHolder();
            if (holder == null) {
                holder = new FriendViewHolder(view);
                view.applyStyleColors(holder);

                view.setViewHolder(holder);
            }

            return holder;

        }

        @Override
        public void onBindViewHolder(@NonNull FriendViewHolder viewHolder, final int position) {

            final Contact contact = mItemList.get(position);

            viewHolder.contactId =  -1;
            viewHolder.address =  contact.getAddress().getBareAddress();
            viewHolder.type = 0;

            String nickname = contact.getName();

            viewHolder.nickname = nickname;

            /**
            if (viewHolder.itemView instanceof ContactListItem) {

               ((ContactListItem)viewHolder.itemView).bind(viewHolder, contact, new View.OnClickListener() {
                   @Override
                   public void onClick(View view) {
                        NearbyAddContactActivity.confirmContact(contact);

                   }
               }, new View.OnClickListener() {
                   @Override
                   public void onClick(View view) {
                        NearbyAddContactActivity.ignoreContact(contact);
                   }
               });
            }

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });
            **/
        }

        @Override
        public int getItemCount() {
            return mItemList.size();
        }


    }
}
