package info.guardianproject.keanuapp.ui.lock

import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import com.beautycoder.pflockscreen.PFFLockScreenConfiguration
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment.OnPFLockScreenCodeCreateListener
import com.beautycoder.pflockscreen.security.PFSecurityManager
import com.beautycoder.pflockscreen.viewmodels.PFPinCodeViewModel
import info.guardianproject.keanu.core.KeanuConstants
import info.guardianproject.keanuapp.R

const val ACTION_CHANGE_PASSPHRASE = "cp"


class AppLockScreenActivity : AppCompatActivity() {

    private val mPrefs: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(this)
    private val mCodeCreateListener: OnPFLockScreenCodeCreateListener =
        object : OnPFLockScreenCodeCreateListener {
            override fun onCodeCreated(encodedCode: String) {
                savePinCode(encodedCode)
                val intent = Intent()
                setResult(RESULT_OK,intent)
                finish()
            }

            override fun onNewCodeValidationFailed() {
                Toast.makeText(
                    this@AppLockScreenActivity,
                    R.string.lock_screen_passphrases_not_matching,
                    Toast.LENGTH_SHORT
                )
                    .show()
                vibrateOnError()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_lock_screen)
        if (intent != null && intent.action != null) {
            val mAction = intent.action
            if (mAction.equals(ACTION_CHANGE_PASSPHRASE, true)) {
                resetPinCode()

            }
        } else {
            showLockScreenView()
        }

    }

    private fun showLockScreenView() {
        PFPinCodeViewModel().isPinCodeEncryptionKeyExist.observe(this) { pinCodeResult ->
            if (pinCodeResult == null) {
                return@observe
            }
            if (pinCodeResult.error != null) {
                Toast.makeText(this, getString(R.string.pincodeNotFound), Toast.LENGTH_SHORT)
                    .show()
                return@observe
            }
            showCreateLockScreenView(pinCodeResult.result)

        }
    }

    private fun showCreateLockScreenView(isPinExist: Boolean) {
        val builder = PFFLockScreenConfiguration.Builder(this)
            .setTitle(if (isPinExist) getString(R.string.title_activity_lock_screen) else getString(R.string.lock_screen_passphrase_not_set_enter))
            .setCodeLength(6)
            .setNewCodeValidation(true)
            .setNewCodeValidationTitle(getString(R.string.lock_screen_passphrases_not_matching))
            .setUseFingerprint(true)
            .setErrorVibration(true)
            .setClearCodeOnError(true)
            .setErrorAnimation(true)
        val lockFragment = PFLockScreenFragment()

        builder.setMode(PFFLockScreenConfiguration.MODE_CREATE)
        lockFragment.setConfiguration(builder.build())
        lockFragment.setCodeCreateListener(mCodeCreateListener)
        supportFragmentManager.beginTransaction()
            .replace(R.id.lockContainerLayout, lockFragment).commit()

    }

    private fun savePinCode(encryptedCode: String) {
        val editor = mPrefs.edit()
        editor.putString(KeanuConstants.PREFERENCE_KEY_ENCODED_PASS, encryptedCode)
        editor.apply()

    }

    private fun deletePinCode() {
        PFSecurityManager.getInstance().pinCodeHelper.delete { result ->
            if (result == null || result.error != null) {
                Toast.makeText(this, getString(R.string.general_error,result.error), Toast.LENGTH_LONG).show()
                vibrateOnError()
            } else {
                val editor = mPrefs.edit()
                editor.remove(KeanuConstants.PREFERENCE_KEY_ENCODED_PASS)
                editor.apply()
            }
        }

    }

    private fun resetPinCode() {
        deletePinCode()
        showCreateLockScreenView(false)
    }

    private fun vibrateOnError() {
        val vibrator = getSystemService(VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            //deprecated in API 26
            vibrator.vibrate(500)
        }
    }
}

