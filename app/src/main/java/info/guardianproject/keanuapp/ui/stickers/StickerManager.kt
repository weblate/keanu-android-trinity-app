package info.guardianproject.keanuapp.ui.stickers

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.BitmapFactory
import android.net.Uri
import android.text.Spannable
import android.text.style.ImageSpan
import timber.log.Timber
import java.io.IOException
import java.lang.ref.WeakReference
import java.util.*
import java.util.regex.Pattern

class StickerManager private constructor(context: Context) {

    companion object {
        private var sInstance: StickerManager? = null

        private const val PLUGIN_CONSTANT = "info.guardianproject.keanu.emoji.STICKER_PACK"

        @JvmStatic
        @Synchronized
        fun getInstance(context: Context): StickerManager {
            if (sInstance == null) {
                sInstance = StickerManager(context).initStickers(context)
            }

            return sInstance!!
        }
    }

    private val mContext = WeakReference(context.applicationContext)


    private val emoticons: MutableMap<Pattern, Sticker> = HashMap()
    private val categories = LinkedHashMap<String, StickerGroup>()

    @Throws(IOException::class)
    fun addJsonPlugins() {
        val packageManager = mContext.get()?.packageManager
        val stickerIntent = Intent(PLUGIN_CONSTANT)
        val stickerPack = packageManager?.queryIntentActivities(stickerIntent, 0) ?: emptyList()

        for (ri in stickerPack) {
            try {
                val res = packageManager?.getResourcesForApplication(ri.activityInfo.applicationInfo)
                val files = res?.assets?.list("") ?: emptyArray()

                for (file in files) {
                    if (file.endsWith(".json")) addJsonDefinitions(file, file.substring(0, file.length - 5), "png", res)
                }
            }
            catch (e: PackageManager.NameNotFoundException) {
                Timber.e("Unable to find application for emoji plugin.")
            }
        }
    }

    @JvmOverloads
    @Throws(IOException::class)
    fun addJsonDefinitions(assetPathJson: String, basePath: String?, fileExt: String?, res: Resources? = mContext.get()?.resources) {
//        val gson = Gson()
//
//        val reader = res?.assets?.open(assetPathJson)?.reader()
//
//        val collectionType = TypeToken<ArrayList<Emoji>>(){}.type
//
//        val emojis = gson.fromJson<Collection<Emoji>>(reader, collectionType)
//
//        for (emoji in emojis) {
//            emoji.assetPath = basePath + '/' + emoji.name + '.' + fileExt
//            emoji.res = res;
//
//            try {
//                res?.assets?.open(emoji.assetPath)
//
//                addPattern(':' + emoji.name + ':', emoji)
//
//                if (emoji.moji != null)
//                    addPattern(emoji.moji, emoji)
//
//                if (emoji.emoticon != null)
//                    addPattern(emoji.emoticon, emoji)
//
//
//                if (emoji.category != null)
//                    addEmojiToCategory (emoji.category, emoji)
//            }
//            catch (ignored: FileNotFoundException) {
//                // Should not be added as a valid emoji.
//            }
//        }
    }

    val emojiGroups: Collection<StickerGroup>
        get() = categories.values

    fun getAssetPath(emoji: Sticker): String {
        return emoji.name
    }

    @Synchronized
    fun addEmojiToCategory(category: String, emoji: Sticker?) {
        var emojiGroup = categories[category]

        if (emojiGroup == null) {
            emojiGroup = StickerGroup()
            emojiGroup.category = category
            emojiGroup.emojis = ArrayList()
        }

        emojiGroup.emojis.add(emoji)

        categories[category] = emojiGroup
    }

    fun addPattern(pattern: String, resource: Sticker) {
        emoticons[Pattern.compile(pattern, Pattern.LITERAL)] = resource
    }

    fun addPattern(charPattern: Char, resource: Sticker) {
        emoticons[Pattern.compile(charPattern.toString() + "", Pattern.UNICODE_CASE)] = resource
    }

    @Throws(IOException::class)
    fun addEmoji(context: Context, spannable: Spannable): Boolean {
        var hasChanges = false

        for ((key, emoji) in emoticons) {
            val matcher = key.matcher(spannable)

            while (matcher.find()) {
                var set = true

                for (span in spannable.getSpans(matcher.start(), matcher.end(), ImageSpan::class.java)) {
                    if (spannable.getSpanStart(span) >= matcher.start() && spannable.getSpanEnd(span) <= matcher.end()) {
                        spannable.removeSpan(span)
                    }
                    else {
                        set = false
                        break
                    }
                }

                if (set) {
                    hasChanges = true
                    spannable.setSpan(
                            ImageSpan(context, BitmapFactory.decodeStream(emoji.res.assets.open(emoji.assetUri.path!!))),
                            matcher.start(), matcher.end(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                }
            }
        }

        return hasChanges
    }

    private fun initStickers(context: Context): StickerManager {
        try {
            val assets = context.assets
            val basePath = "stickers"
            val stickers = assets.list(basePath)

            for (stickerPack in stickers ?: emptyArray()) {
                assets.list("$basePath/$stickerPack")?.let {
                    loadStickers(context, stickerPack, "$basePath/$stickerPack", it)
                }
            }
        }
        catch (e: Exception) {
            Timber.e(e, "Could not load emoji definition.")
        }

        return this
    }

    private fun loadStickers(context: Context, category: String, basePath: String, files: Array<String>) {
        for (file in files) {
            val sticker = Sticker()

            sticker.name = file
            sticker.category = category
            sticker.assetUri = Uri.parse(basePath + '/' + file)
            sticker.res = context.resources
            sticker.emoticon = file

            addPattern(sticker.emoticon, sticker)

            addEmojiToCategory(category, sticker)
        }
    }
}