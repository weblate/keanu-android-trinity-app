package info.guardianproject.keanuapp.ui.qr;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ui.BaseActivity;
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager;
import kotlin.text.Charsets;

import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_NOSENSOR;

@SuppressWarnings("deprecation")
@SuppressLint("LogNotTimber")
public class QrScanActivity extends BaseActivity implements QrCodeDecoder.ResultCallback {

	public static final String EXTRA_FINISH_ON_FIRST = "extra_finish_on_first";
    public static final String EXTRA_GET_RAW = "extra_get_raw";

    private static final String TAG = "Qrcode";
    private final static int MY_PERMISSIONS_REQUEST_CAMERA = 1;

    private final Intent mDataResult = new Intent();
    private final ArrayList<String> mResultStrings = new ArrayList<>();
    private CameraView mCameraView = null;
    private View mLayoutMain = null;
    private Camera mCamera = null;

    private boolean mFinishOnFirst = false;
    private boolean mGetRaw = false;

    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);

        setRequestedOrientation(SCREEN_ORIENTATION_NOSENSOR);

        getSupportActionBar().hide();

        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);

        if (permissionCheck == PackageManager.PERMISSION_DENIED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                finish();
            }
            else {
                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
        }
        else {
            init();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String [] permissions, int [] grantResults) {
		if (requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {
			if (grantResults.length > 0
					&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				init();
			}
			else {
				finish();
			}

			return;
		}

		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void init() {
        setContentView(R.layout.awesome_activity_scan);

        mLayoutMain = findViewById(R.id.layout_main);

        LinearLayout cameraBorder = (LinearLayout) findViewById(R.id.camera_box);
        mCameraView = new CameraView(this);
        cameraBorder.addView(mCameraView);

        Intent intent = getIntent();
        String qrData = null;

        if (intent != null) {
            qrData = intent.getStringExtra(Intent.EXTRA_TEXT);
            mFinishOnFirst = intent.getBooleanExtra(EXTRA_FINISH_ON_FIRST, false);
            mGetRaw = intent.getBooleanExtra(EXTRA_GET_RAW, false);
        }


		if (!TextUtils.isEmpty(qrData)) {
            ImageView qrCodeView = (ImageView) findViewById(R.id.qr_box_image);

            new QrGenAsyncTask(this, qrCodeView, 100).execute(qrData);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        openCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();

        releaseCamera();
    }

    private synchronized void openCamera() {
        Log.d(TAG, "Opening camera");

        if (mCamera == null) {
            try {
                mCamera = Camera.open();

                mCameraView.start(mCamera, new QrCodeDecoder(this), 0, true);
            }
            catch (Exception e) {
                Log.e(TAG, "Error opening camera", e);

            }
        }
    }

    private void releaseCamera() {
        Log.d(TAG, "Releasing camera");

        try {
            mCameraView.stop();
            mCamera.release();
            mCamera = null;
        }
        catch (Exception e) {
            Log.e(TAG, "Error releasing camera", e);

        }
    }

    public void handleResult(final Result result) {
        runOnUiThread(() -> {

			String resultString = mGetRaw ? new String(getRawBytes(result), Charsets.ISO_8859_1) : result.getText();

			if (!mResultStrings.contains(resultString)) {
				mResultStrings.add(resultString);
				mDataResult.putStringArrayListExtra("result", mResultStrings);

				OnboardingManager.DecodedInviteLink diLink = OnboardingManager.decodeInviteLink(resultString);

				String message = null;

				if (diLink != null) {
					message = getString(R.string.add_contact_success, diLink.username);
				}

				if (message != null) {
					Snackbar.make(mLayoutMain, message, Snackbar.LENGTH_LONG).show();
				}

				setResult(RESULT_OK, mDataResult);

				if (mFinishOnFirst) {
					finish();
				}
			}
		});
    }

    // Copied from https://github.com/markusfisch/BinaryEye/blob/
    // 9d57889b810dcaa1a91d7278fc45c262afba1284/app/src/main/kotlin/de/markusfisch/android/binaryeye/activity/CameraActivity.kt#L434
    private byte[] getRawBytes(Result result) {
        Map<ResultMetadataType, Object> metadata = result.getResultMetadata();
        if (metadata == null) return  null;

        @SuppressWarnings("unchecked")
        List<byte[]> segments = (List<byte[]>) metadata.get(ResultMetadataType.BYTE_SEGMENTS);
        if (segments == null) return null;

        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        for (byte[] s : segments) {
            stream.write(s, 0, s.length);
        }

        byte[] bytes = stream.toByteArray();

        // Byte segments can never be shorter than the text.
        // Zxing cuts off content prefixes like "WIFI:"
        if (bytes.length >= result.getText().length()) return bytes;

        return null;
    }
}