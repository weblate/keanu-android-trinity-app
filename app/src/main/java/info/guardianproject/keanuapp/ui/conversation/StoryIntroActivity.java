package info.guardianproject.keanuapp.ui.conversation;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Date;
import java.util.List;

import info.guardianproject.keanu.core.ImApp;
import info.guardianproject.keanu.core.model.Contact;
import info.guardianproject.keanu.core.model.ImErrorInfo;
import info.guardianproject.keanu.core.service.IChatSession;
import info.guardianproject.keanu.core.service.IChatSessionListener;
import info.guardianproject.keanu.core.service.IChatSessionManager;
import info.guardianproject.keanu.core.service.IImConnection;
import info.guardianproject.keanu.core.ui.room.RoomActivity;
import info.guardianproject.keanu.core.ui.room.StoryActivity;
import info.guardianproject.keanu.core.util.LogCleaner;
import info.guardianproject.keanuapp.R;
import timber.log.Timber;

import static info.guardianproject.keanu.core.KeanuConstants.LOG_TAG;

public class StoryIntroActivity extends AppCompatActivity {

    private Handler mHandler = new Handler();
    private IChatSession mCurrentChatSession;
    private IImConnection mConn;

    long mLastChatId=-1;
    String mRemoteNickname;
    String mRemoteAddress;

    long mProviderId = -1;
    long mAccountId = -1;
    long mInvitationId;
    private int mPresenceStatus;
    private Date mLastSeen;

    private int mViewType;
    private int mContactType;

    private View mProgressBar;
    private View mGroupLaunch;

    private ImApp getApp() {
        return (ImApp) getApplication();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.awesome_activity_story_intro);

        mProgressBar = findViewById(R.id.progressBar);
        mGroupLaunch = findViewById(R.id.groupLaunch);

        getSupportActionBar().hide();
        setupStoryMode();
    }

    private void setupStoryMode () {
        mLastChatId = getIntent().getLongExtra("id", -1);
        mRemoteAddress = getIntent().getStringExtra(RoomActivity.EXTRA_ROOM_ID);

        bindQuery(mLastChatId);
    }

    private void launchStoryMode (String mode) {

        final Intent intent = getApp().getRouter().story(this, mRemoteAddress, null, mode.equals("contrib"));

        // TODO: These don't seem to be used!
        intent.putExtra("id", mLastChatId);
        intent.putExtra("nickname", getIntent().getStringExtra("nickname"));

        if (!TextUtils.isEmpty(mode))
        {
            startActivity(intent);
            finish();
        }
        else {

            getChatSession(new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] objects) {

                    List<Contact> admins = null;
                    try {

                        mCurrentChatSession.refreshContactFromServer();

                        boolean isContrib = false;

                        admins = mCurrentChatSession.getGroupChatAdmins();

                        if (admins != null) {
                            for (Contact c : admins) {
                                if (c.getAddress().getBareAddress().equals(mLocalAddress)) {
                                    isContrib = true;
                                    break;

                                }
                            }
                        }

                        if (!isContrib) {
                            admins = mCurrentChatSession.getGroupChatOwners();

                            if (admins != null) {
                                for (Contact c : admins) {
                                    if (c.getAddress().getBareAddress().equals(mLocalAddress)) {
                                        isContrib = true;
                                        break;

                                    }
                                }
                            }
                        }

                        intent.putExtra(StoryActivity.EXTRA_CONTRIBUTOR_MODE, isContrib);
                        startActivity(intent);
                        finish();


                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    return null;
                }
            });
        }

    }

    private boolean bindQuery (long chatId) {

       /**
        if (c != null && (!c.isClosed()) && c.getCount() > 0)
        {

            initData();

            if (!hasJoined())
            {
                mHandler.post(() -> showJoinGroupUI());
            }
            else
            {
                mHandler.post(() -> launchStoryMode(null));
            }

        }


        c.close();
        **/

        initSession ();


        return true;


    }

    private void initSession ()
    {
        mCurrentChatSession = getChatSession(null);
        if (mCurrentChatSession == null)
            createChatSession();
    }

    private void createChatSession() {

        try
        {
            if (mConn != null) {
                IChatSessionManager sessionMgr = mConn.getChatSessionManager();
                if (sessionMgr != null) {
                    sessionMgr.createChatSession(mRemoteAddress, false, new IChatSessionListener() {
                        @Override
                        public void onChatSessionCreated(IChatSession session) throws RemoteException {
                            mCurrentChatSession = session;
                        }

                        @Override
                        public void onChatSessionCreateError(String name, ImErrorInfo error) throws RemoteException {

                        }

                        @Override
                        public IBinder asBinder() {
                            return null;
                        }
                    });


                }
            }

        } catch (Exception e) {

            //mHandler.showServiceErrorAlert(e.getLocalizedMessage());
            LogCleaner.error(LOG_TAG, "issue getting chat session", e);
        }

    }
    
    public IChatSession getChatSession(AsyncTask task) {

        try {

            if (mCurrentChatSession != null) {
                if (task != null)
                    task.execute();
                return mCurrentChatSession;
            }
            else if (mConn != null)
            {
                IChatSessionManager sessionMgr = mConn.getChatSessionManager();
                if (sessionMgr != null) {

                    IChatSession session = sessionMgr.getChatSession(mRemoteAddress);

                    if (session == null) {
                        sessionMgr.createChatSession(mRemoteAddress, false, new IChatSessionListener() {
                            @Override
                            public void onChatSessionCreated(IChatSession session) throws RemoteException {
                                mCurrentChatSession = session;

                                if (task != null)
                                    task.execute();
                            }

                            @Override
                            public void onChatSessionCreateError(String name, ImErrorInfo error) throws RemoteException {

                            }

                            @Override
                            public IBinder asBinder() {
                                return null;
                            }
                        });
                    }
                    else
                    {
                        mCurrentChatSession = session;
                        if (task != null)
                            task.execute();
                    }

                    return session;

                }
            }


        } catch (Exception e) {

            //mHandler.showServiceErrorAlert(e.getLocalizedMessage());
            LogCleaner.error(LOG_TAG, "error getting chat session", e);
        }

        return null;
    }

    private boolean hasJoined ()
    {
        return true;//TODO
    }

    private void showJoinGroupUI ()
    {
        final View joinGroupView = findViewById(R.id.join_group_view);

        if (joinGroupView != null) {
            joinGroupView.setVisibility(View.VISIBLE);

            mProgressBar.setVisibility(View.GONE);

            final View btnJoinAccept = joinGroupView.findViewById(R.id.btnJoinAccept);
            final View btnJoinDecline = joinGroupView.findViewById(R.id.btnJoinDecline);
            final TextView title = joinGroupView.findViewById(R.id.room_join_title);

            title.setText(title.getContext().getString(R.string.room_invited));

            btnJoinAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getChatSession(new AsyncTask() {
                        @Override
                        protected Object doInBackground(Object[] objects) {

                            getChatSession(new AsyncTask() {
                                @Override
                                protected Object doInBackground(Object[] objects) {

                                    if (mCurrentChatSession != null) {
                                        setGroupSeen();
                                        launchStoryMode(null);
                                    } else {
                                        finish();
                                    }


                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Object o) {
                                    super.onPostExecute(o);

                                }
                            });


                            return null;
                        }

                        @Override
                        protected void onPostExecute(Object o) {
                            super.onPostExecute(o);

                            joinGroupView.setVisibility(View.GONE);
                        }
                    });
                }
            });
            btnJoinDecline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getChatSession(new AsyncTask() {
                        @Override
                        protected Object doInBackground(Object[] objects) {

                            if (mCurrentChatSession != null) {
                                try {
                                    mCurrentChatSession.leave();

                                } catch (RemoteException re) {
                                }
                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Object o) {
                            super.onPostExecute(o);

                            // Clear the stack and go back to the main activity.
                            Intent intent = StoryIntroActivity.this.getApp().getRouter().main(v.getContext());
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            v.getContext().startActivity(intent);
                        }
                    });


                }
            });
        }
        else
        {

        }

    }

    private void setGroupSeen() {

        try {
            if (mCurrentChatSession != null)
                mCurrentChatSession.markAsSeen();


        }
        catch (RemoteException re)
        {
            Timber.e(re, "error setting subscription / markAsSeen()");
        }
    }

    private String mLocalAddress;

    private void initData () {



    }


    public void shareStory(View view) {
        launchStoryMode("contrib");
    }

    public void viewStory(View view) {
        launchStoryMode("viewer");

    }
}
