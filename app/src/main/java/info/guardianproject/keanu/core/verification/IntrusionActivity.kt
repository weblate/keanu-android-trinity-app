package info.guardianproject.keanu.core.verification

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityIntrusionBinding

class IntrusionActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var mBinding: ActivityIntrusionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityIntrusionBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.btGoToProfile.setOnClickListener(this)
        mBinding.btIgnore.setOnClickListener(this)

        supportActionBar?.title = getString(R.string.New_Log_In)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        if (v == mBinding.btGoToProfile) {

            val intent = (application as? ImApp)?.router?.main(this, preselectTab = 3)
            intent?.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

            startActivity(intent)
        }

        finish()
    }
}