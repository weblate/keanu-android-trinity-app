package info.guardianproject.keanu.core.util

import android.content.ContentResolver
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Build
import android.provider.OpenableColumns
import androidx.exifinterface.media.ExifInterface
import org.matrix.android.sdk.api.session.content.ContentAttachmentData
import timber.log.Timber

@Suppress("MemberVisibilityCanBePrivate")
object AttachmentHelper {

    /**
     * Creates a [ContentAttachmentData] object with all necessary arguments from
     * a given content URI.
     *
     * @param cr
     *      A [ContentResolver] able to resolve the given URI. (Typically [Context.contentResolver].)
     *
     * @param contentUri
     *      A "content://" URI of an audio or video file.
     *
     * @param fallbackMimeType
     *      A MIME type to fall back on, in case [ContentResolver.getType()] evaluation fails. OPTIONAL.
     *
     * @return
     *      A [ContentAttachmentData] object with all parameters set correctly according to file type.
     */
    fun createFromContentUri(cr: ContentResolver, contentUri: Uri, fallbackMimeType: String? = null): ContentAttachmentData {
        val mimeType = cr.getType(contentUri) ?: fallbackMimeType

        var name: String? = null
        var size = 0L

        cr.query(contentUri, null, null, null, null)?.use {
            it.moveToFirst()

            try {
                name = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
            }
            catch (failure: Throwable) {
                Timber.e(failure, "Cannot read name of file: %s", contentUri.toString())
            }

            try {
                size = it.getLong(it.getColumnIndex(OpenableColumns.SIZE))
            }
            catch (failure: Throwable) {
                Timber.e(failure, "Cannot read size of file: %s", contentUri.toString())
            }
        }

        when {
            mimeType?.startsWith("audio") == true -> {
                val (duration) = getMetadata(cr, contentUri)

                return ContentAttachmentData(size = size, duration = duration, name = name,
                        queryUri = contentUri,  mimeType = mimeType,
                        type = ContentAttachmentData.Type.AUDIO)
            }

            mimeType?.startsWith("video") == true -> {
                val (duration, width, height, orientation) = getMetadata(cr, contentUri)

                return ContentAttachmentData(size = size, duration = duration, height = height,
                        width = width, exifOrientation = orientation.toInt(), name = name,
                        queryUri = contentUri,  mimeType = mimeType,
                        type = ContentAttachmentData.Type.VIDEO)
            }

            mimeType?.startsWith("image") == true -> {
                val bitmap = getBitmap(cr, contentUri)
                val orientation = getOrientation(cr, contentUri)

                return ContentAttachmentData(size = size, height = bitmap?.height?.toLong() ?: 0,
                        width = bitmap?.width?.toLong() ?: 0, exifOrientation = orientation,
                        name = name, queryUri = contentUri, mimeType = mimeType,
                        type = ContentAttachmentData.Type.IMAGE)
            }

            else -> {
                return ContentAttachmentData(size = size, name = name, queryUri = contentUri,
                        mimeType = mimeType, type = ContentAttachmentData.Type.FILE)
            }
        }
    }

    /**
     * Reads some metadata form an audio or video file.
     *
     * @param cr
     *      A [ContentResolver] able to resolve the given URI. (Typically [Context.contentResolver].)
     *
     * @param contentUri
     *      A "content://" URI of an audio or video file.
     *
     * @return
     *      A list of 4 values:
     *      2. duration in MS
     *      3. width in pixels (video only)
     *      4. height in pixels (video only)
     *      5. orientation in degrees (0, 90, 180, 270, video only)
     */
    fun getMetadata(cr: ContentResolver, contentUri: Uri): List<Long> {
        val result = arrayListOf(0L, 0L, 0L, 0L)

        cr.openFileDescriptor(contentUri, "r")?.use { parcelFileDescriptor ->
            val mmr = MediaMetadataRetriever()
            mmr.setDataSource(parcelFileDescriptor.fileDescriptor)

            mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)?.toLong()?.let {
                result[0] = it
            }

            mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH)?.toLong()?.let {
                result[1] = it
            }

            mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT)?.toLong()?.let {
                result[2] = it
            }

            mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION)?.toLong()?.let {
                result[3] = it
            }
        }

        return result
    }

    /**
     * Reads an image from a content URI.
     *
     * @param cr
     *      A [ContentResolver] able to resolve the given URI. (Typically [Context.contentResolver].)
     *
     * @param contentUri
     *      A "content://" URI of an image file.
     *
     * @return
     *      The decoded image or NULL if an error happened.
     *
     */
    fun getBitmap(cr: ContentResolver, contentUri: Uri): Bitmap? {
        return try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                ImageDecoder.decodeBitmap(ImageDecoder.createSource(cr, contentUri))
            } else {
                cr.openInputStream(contentUri)?.use {
                    BitmapFactory.decodeStream(it)
                }
            }
        }
        catch (failure: Throwable) {
            Timber.e(failure, "Cannot decode Bitmap: %s", contentUri.toString())

            null
        }
    }

    /**
     * Reads the image orientation from EXIF data of a content URI.
     *
     * @param cr
     *      A [ContentResolver] able to resolve the given URI. (Typically [Context.contentResolver].)
     *
     * @param contentUri
     *      A "content://" URI of an image file.
     *
     * @return
     *      Image orientation in degrees: 0, 90, 180, 270. Will also return 0, if an error happened.
     */
    fun getOrientation(cr: ContentResolver, contentUri: Uri): Int {
        var orientation = 0

        cr.openInputStream(contentUri)?.use { inputStream ->
            try {
                ExifInterface(inputStream).let {
                    orientation = it.rotationDegrees
                }
            }
            catch (failure: Throwable) {
                Timber.e(failure, "Cannot read orientation: %s", contentUri.toString())
            }
        }

        return orientation
    }
}