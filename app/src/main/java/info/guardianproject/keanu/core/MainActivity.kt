package info.guardianproject.keanu.core

import android.app.SearchManager
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.beautycoder.pflockscreen.PFFLockScreenConfiguration
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment.OnPFLockScreenLoginListener
import com.beautycoder.pflockscreen.security.PFSecurityManager
import com.beautycoder.pflockscreen.viewmodels.PFPinCodeViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.google.android.material.tabs.TabLayout.TabLayoutOnPageChangeListener
import info.guardianproject.keanu.core.KeanuConstants.PREFERENCE_KEY_ENCODED_PASS
import info.guardianproject.keanu.core.ui.MoreFragment
import info.guardianproject.keanu.core.ui.chatlist.ChatListFragment
import info.guardianproject.keanu.core.ui.friends.FriendsListFragment
import info.guardianproject.keanu.core.ui.friends.FriendsPickerActivity
import info.guardianproject.keanu.core.ui.me.MyProfileFragment
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanu.core.util.extensions.copy
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.AwesomeActivityMainBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.room.model.RoomHistoryVisibility
import org.matrix.android.sdk.api.session.room.model.create.CreateRoomParams
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.*


class MainActivity : BaseActivity() {

    companion object {
        const val EXTRA_PRESELECT_TAB = "preselect_tab"

        const val EXTRA_JOIN_ROOM_ID = "join-room-id"

        const val EXTRA_INVITE_USER_ID = "invite-user-id"

        const val EXTRA_OPEN_ROOM_ID = "open-room-id"

        const val EXTRA_IS_FIRST_TIME = "is-first-time"

        fun getContrastColor(colorIn: Int): Int {
            val y =
                (299 * Color.red(colorIn) + 587 * Color.green(colorIn) + 114 * Color.blue(colorIn)) / 1000.0
            return if (y >= 128) Color.BLACK else Color.WHITE
        }
    }

    private val mApp: ImApp?
        get() = application as? ImApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private val mPrefs: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(this)

    private var mSbStatus: Snackbar? = null

    private lateinit var mBinding: AwesomeActivityMainBinding

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mBinding.root))
    }

    private val mLoginListener: OnPFLockScreenLoginListener = object : OnPFLockScreenLoginListener {
        override fun onCodeInputSuccessful() {
            showHomePage()
        }

        override fun onFingerprintSuccessful() {
            showHomePage()
        }

        override fun onPinLoginFailed() {

            Toast.makeText(this@MainActivity, "Wrong PIN", Toast.LENGTH_SHORT).show()
        }

        override fun onFingerprintLoginFailed() {

            Toast.makeText(this@MainActivity, "Not recognized", Toast.LENGTH_SHORT).show()
        }
    }

    val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                invalidateOptionsMenu()
            }

        }


    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Preferences.doBlockScreenshots()) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE
            )
        }

        mBinding = AwesomeActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.bannerClose.setOnClickListener { mBinding.bannerLayout.visibility = View.GONE }

        setSupportActionBar(mBinding.toolbar)

        val tabChangeListener = TabLayoutOnPageChangeListener(mBinding.tabs)

        mBinding.viewpager.adapter = FragmentAdapter(this)
        mBinding.viewpager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrollStateChanged(state: Int) {
                tabChangeListener.onPageScrollStateChanged(state)
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                tabChangeListener.onPageScrolled(position, positionOffset, positionOffsetPixels)
            }

            override fun onPageSelected(position: Int) {
                tabChangeListener.onPageSelected(position)
            }
        })

        var tab = mBinding.tabs.newTab()
        tab.setIcon(R.drawable.ic_discuss)
        tab.tag = getString(R.string.chats)
        tab.setContentDescription(R.string.chats)
        mBinding.tabs.addTab(tab)

        tab = mBinding.tabs.newTab()
        tab.setIcon(R.drawable.ic_people_white_36dp)
        tab.tag = getString(R.string.contacts)
        tab.setContentDescription(R.string.contacts)
        mBinding.tabs.addTab(tab)

        tab = mBinding.tabs.newTab()
        tab.setIcon(R.drawable.ic_explore_white_24dp)
        tab.tag = getString(R.string.title_more)
        tab.setContentDescription(R.string.title_more)
        mBinding.tabs.addTab(tab)

        tab = mBinding.tabs.newTab()
        tab.setIcon(R.drawable.ic_face_white_24dp)
        tab.tag = getString(R.string.title_me)
        tab.setContentDescription(R.string.title_me)
        mBinding.tabs.addTab(tab)

        mBinding.tabs.addOnTabSelectedListener(object : OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab) {
                mBinding.viewpager.currentItem = tab.position
                setToolbarTitle(tab.position)
                applyStyle()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {
                setToolbarTitle(tab.position)
                applyStyle()
            }
        })

        mBinding.fab.setOnClickListener {
            when (mBinding.viewpager.currentItem) {
                0 -> {
                    if (getFragment<FriendsListFragment>()?.friendCount ?: 0 > 0) {
                        mChooseContact.launch(mApp?.router?.friendsPicker(this@MainActivity))
                    } else {
                        inviteFriend()
                    }
                }

                1 -> inviteFriend()
            }
        }

        setToolbarTitle(0)

        applyStyle()

        if (mSession == null) {
            startActivity(mApp?.router?.onboarding(this))

            finish()
        }
        checkAndShowLockScreenFragment()
    }

    private fun setToolbarTitle(tabPosition: Int) {
        val sb = StringBuilder()
        sb.append(getString(R.string.app_name))
        sb.append(" | ")

        when (tabPosition) {
            0 -> if (getFragment<ChatListFragment>()?.showArchived == true) sb.append(getString(R.string.action_archive)) else sb.append(
                getString(R.string.chats)
            )

            1 -> sb.append(getString(R.string.friends))

            2 -> sb.append(getString(R.string.title_more))

            3 -> {
                sb.append(getString(R.string.me_title))
            }
        }

        mBinding.toolbar.title = sb.toString()

        when (tabPosition) {
            1 -> {
                mBinding.fab.setImageResource(R.drawable.ic_person_add_white_36dp)
                mBinding.fab.visibility = View.VISIBLE
            }

            2, 3 -> mBinding.fab.visibility = View.GONE

            else -> {
                mBinding.fab.setImageResource(R.drawable.ic_add_white_24dp)
                mBinding.fab.visibility = View.VISIBLE
            }
        }
    }

    fun inviteFriend() {
        mAddFriend.launch(mApp?.router?.addFriend(this@MainActivity))
    }

    /**
     * check if the log is enables or not
     */
    private fun checkAndShowLockScreenFragment() {
        PFPinCodeViewModel().isPinCodeEncryptionKeyExist.observe(
            this,
            androidx.lifecycle.Observer { result ->
                if (result == null) {
                    return@Observer
                }
                if (result.error != null) {
                    Toast.makeText(
                        this@MainActivity,
                        "Can not get pin code info",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@Observer
                }
                showLockScreenFragment(result.result)
            }
        )
    }


    /**
     * show or hide the lock screen
     */
    private fun showLockScreenFragment(isPinExist: Boolean) {
        if (isPinExist) {
            mBinding.containerView.visibility = View.VISIBLE
            mBinding.viewpager.visibility = View.GONE
        } else {
            mBinding.containerView.visibility = View.GONE
            mBinding.viewpager.visibility = View.VISIBLE
        }
        val builder = PFFLockScreenConfiguration.Builder(this)
            .setTitle(getString(R.string.lock_screen_passphrase_not_set_enter))
            .setCodeLength(6)
            .setNewCodeValidation(true)
            .setNewCodeValidationTitle(getString(R.string.lock_screen_confirm_passphrase))
            .setErrorVibration(true)
            .setClearCodeOnError(true)
            .setErrorAnimation(true)

        if (isPinExist)
            builder.setTitle(getString(R.string.title_activity_lock_screen))
        
        val fragment = PFLockScreenFragment()
        builder.setMode(PFFLockScreenConfiguration.MODE_AUTH)
        fragment.setEncodedPinCode(mPrefs.getString(PREFERENCE_KEY_ENCODED_PASS, ""))
        fragment.setLoginListener(mLoginListener)
        fragment.setConfiguration(builder.build())
        supportFragmentManager.beginTransaction()
            .replace(R.id.container_view, fragment).commit()
    }

    /**
     * show home page by hiding the lock view
     */
    fun showHomePage() {
        mBinding.containerView.visibility = View.GONE
        mBinding.viewpager.visibility = View.VISIBLE
    }

    public override fun onResume() {
        super.onResume()

        applyStyle()

        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        setIntent(intent)

        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent?) {
        when {
            intent?.hasExtra(EXTRA_INVITE_USER_ID) == true -> {
                // Create a new room and invite the user to it.
                intent.getStringExtra(EXTRA_INVITE_USER_ID)?.let { createRoom(listOf(it)) }
            }

            intent?.hasExtra(EXTRA_JOIN_ROOM_ID) == true -> {
                intent.getStringExtra(EXTRA_JOIN_ROOM_ID)?.let { joinRoom(it) }
            }

            intent?.hasExtra(FriendsPickerActivity.EXTRA_RESULT_USERNAME) == true -> {
                intent.getStringExtra(FriendsPickerActivity.EXTRA_RESULT_USERNAME)
                    ?.let { createRoom(listOf(it)) }
            }

            intent?.hasExtra(EXTRA_OPEN_ROOM_ID) == true -> {
                intent.getStringExtra(EXTRA_OPEN_ROOM_ID)?.let { openRoom(intent) }
            }

            intent?.getBooleanExtra(EXTRA_IS_FIRST_TIME, false) == true -> {
                inviteFriend()
            }

            intent?.hasExtra(EXTRA_PRESELECT_TAB) == true -> {
                mBinding.tabs.selectTab(
                    mBinding.tabs.getTabAt(
                        intent.getIntExtra(
                            EXTRA_PRESELECT_TAB,
                            0
                        )
                    )
                )
            }
        }

        setIntent(null)
    }

    private val mChangeSettings =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                finish()

                startActivity(mApp?.router?.main(this))
            }
        }

    private val mAddFriend =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                result.data?.getStringExtra(FriendsPickerActivity.EXTRA_RESULT_USERNAME)?.let {
                    createRoom(listOf(it))
                }
            }
        }

    private val mChooseContact =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                val username =
                    result.data?.getStringExtra(FriendsPickerActivity.EXTRA_RESULT_USERNAME)
                if (username != null) {
                    createRoom(listOf(username))
                } else {
                    result.data?.getStringArrayListExtra(FriendsPickerActivity.EXTRA_RESULT_USERNAMES)
                        ?.let {
                            createRoom(it)
                        }
                }
            }
        }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val lockItem = menu.findItem(R.id.menu_lock)
     //   val resetLockItem = menu.findItem(R.id.menu_lock_reset)
        val deleteLockItem = menu.findItem(R.id.menu_lock_delete)
        if (mPrefs.contains(PREFERENCE_KEY_ENCODED_PASS)) {
            lockItem.isVisible = true
       //     resetLockItem.isVisible = true
            deleteLockItem.isVisible = true

        } else {
            lockItem.isVisible = true
         //   resetLockItem.isVisible = false
            deleteLockItem.isVisible = false
        }

        val searchManager = getSystemService(SEARCH_SERVICE) as? SearchManager
        val searchView = menu.findItem(R.id.menu_search).actionView as? SearchView

        searchView?.setSearchableInfo(searchManager?.getSearchableInfo(componentName))
        searchView?.setIconifiedByDefault(false)

        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(query: String): Boolean {
                if (mBinding.tabs.selectedTabPosition == 0) {
                    getFragment<ChatListFragment>()?.doSearch(query)
                } else if (mBinding.tabs.selectedTabPosition == 1) {
                    getFragment<FriendsListFragment>()?.doSearch(query)
                }

                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                if (mBinding.tabs.selectedTabPosition == 0) {
                    getFragment<ChatListFragment>()?.doSearch(query)
                } else if (mBinding.tabs.selectedTabPosition == 1) {
                    getFragment<FriendsListFragment>()?.doSearch(query)
                }

                return true
            }
        })

        searchView?.setOnCloseListener {
            getFragment<ChatListFragment>()?.doSearch(null)
            false
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> return true

            R.id.menu_settings -> {
                mChangeSettings.launch(mApp?.router?.settings(this))
                return true
            }

            R.id.menu_list_normal -> {
                clearFilters()
                Timber.v("Filter: Normal")
                return true
            }

            R.id.menu_list_archive -> {
                Timber.v("Filter: Archive")
                enableArchiveFilter()
                return true
            }

            R.id.menu_lock -> {
                handleLock()
                return true
            }
            R.id.menu_lock_delete -> {
                disableLock()
                invalidateOptionsMenu()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }


    /**
     * disable lock feature
     */
    private fun disableLock() {
        PFSecurityManager.getInstance().pinCodeHelper.delete { result ->
            if (result == null || result.error != null) {
                Toast.makeText(this, "Issue removing Pin Code", Toast.LENGTH_LONG).show()
            } else {
                val editor = mPrefs.edit()
                editor.remove(PREFERENCE_KEY_ENCODED_PASS)
                editor.apply()
                Toast.makeText(this, "App lock Disabled", Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun clearFilters() {
        if (mBinding.tabs.selectedTabPosition == 0) {
            getFragment<ChatListFragment>()?.showArchived = false
            Timber.v("Filter: clear_1")
        }

        setToolbarTitle(mBinding.tabs.selectedTabPosition)
    }

    private fun enableArchiveFilter() {
        if (mBinding.tabs.selectedTabPosition == 0) {
            getFragment<ChatListFragment>()?.showArchived = true
            Timber.v("Filter: clear_11")
        }

        setToolbarTitle(mBinding.tabs.selectedTabPosition)
    }

    private fun handleLock() {
        if (mPrefs.contains(PREFERENCE_KEY_ENCODED_PASS)) {
            // Reset the passphrase
            //startForResult.launch(mApp?.router?.lockScreen(this, changePassphrase = true))
            finish()
        } else {
            // Need to setup new user passphrase.
            startForResult.launch(mApp?.router?.lockScreen(this, changePassphrase = false))
        }
    }

    fun createRoom(invitees: List<String> = emptyList(), isSession: Boolean = false) {
        mSbStatus = Snackbar.make(
            mBinding.viewpager,
            R.string.loading,
            Snackbar.LENGTH_INDEFINITE
        )
        mSbStatus?.show()

        if (invitees.size == 1) {
            mCoroutineScope.launch {
                val roomId = mSession?.createDirectRoom(invitees.first())
                mSbStatus?.dismiss()
                startActivity(mApp?.router?.room(this@MainActivity, roomId, showRoomInfo = true))
            }
        } else {
            val params = CreateRoomParams()
            params.historyVisibility = RoomHistoryVisibility.INVITED
            params.enableEncryptionIfInvitedUsersSupportIt = true

            mCoroutineScope.launch {
                val roomId = mSession?.createRoom(params)

                val room = roomId?.let { mSession?.getRoom(it) }

                for (invitee in invitees) room?.invite(invitee, "")

                lifecycleScope.launch {
                    mSbStatus?.dismiss()

                    val intent = if (isSession) {
                        mApp?.router?.story(
                            this@MainActivity,
                            roomId = roomId,
                            showRoomInfo = invitees.isEmpty(),
                            contributorMode = true
                        )
                    } else {
                        mApp?.router?.room(
                            this@MainActivity,
                            roomId = roomId,
                            showRoomInfo = invitees.isEmpty()
                        )
                    }

                    startActivity(intent)
                }
            }
        }
    }

    private fun joinRoom(roomId: String) {
        mCoroutineScope.launch {
            mSession?.joinRoom(roomId, "", emptyList())
        }
    }

    private fun openRoom(intent: Intent) {
        startActivity(
            mApp?.router?.room(this, intent.getStringExtra(EXTRA_OPEN_ROOM_ID))?.copy(intent)
        )
    }

    fun applyStyle() {
        // Not set color.
        val themeColorHeader = mPrefs.getInt("themeColor", -1)
        var themeColorText = mPrefs.getInt("themeColorText", -1)
        (mBinding.viewpager.adapter as? FragmentAdapter)?.backgroundColor =
            mPrefs.getInt("themeColorBg", -1)

        if (themeColorHeader != -1) {
            if (themeColorText == -1) themeColorText = getContrastColor(themeColorHeader)

            window.navigationBarColor = themeColorHeader
            window.statusBarColor = themeColorHeader

            @Suppress("DEPRECATION")
            window.setTitleColor(getContrastColor(themeColorHeader))

            mBinding.toolbar.setBackgroundColor(themeColorHeader)
            mBinding.toolbar.setTitleTextColor(getContrastColor(themeColorHeader))
            mBinding.tabs.setBackgroundColor(themeColorHeader)
            mBinding.tabs.setTabTextColors(themeColorText, themeColorText)
            mBinding.fab.setBackgroundColor(themeColorHeader)
        }
    }

    private inline fun <reified F : Fragment> getFragment(): F? {
        return (mBinding.viewpager.adapter as? FragmentAdapter)?.fragments?.firstOrNull { it?.get() is F }
            ?.get() as? F
    }

    internal class FragmentAdapter(activity: MainActivity) : FragmentStateAdapter(activity) {

        private var mFragmentClasses = listOf(
            ChatListFragment::class.java,
            FriendsListFragment::class.java,
            MoreFragment::class.java,
            MyProfileFragment::class.java
        )

        var fragments: ArrayList<WeakReference<Fragment>?> =
            ArrayList(mFragmentClasses.map { null })

        var backgroundColor = -1

        override fun getItemCount(): Int {
            return mFragmentClasses.size
        }

        override fun createFragment(position: Int): Fragment {
            var fragment = fragments[position]?.get()

            if (fragment == null) {
                fragment = mFragmentClasses[position].newInstance()
                fragments[position] = WeakReference(fragment)
            }

            if (backgroundColor > -1) fragment?.requireView()?.setBackgroundColor(backgroundColor)

            return fragment!!
        }

    }
}