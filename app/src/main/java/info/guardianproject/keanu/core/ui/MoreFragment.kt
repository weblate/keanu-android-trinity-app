package info.guardianproject.keanu.core.ui

import android.Manifest.permission
import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import com.google.gson.Gson
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.MainActivity
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentMoreBinding
import info.guardianproject.keanuapp.nearby.AirShareManager
import info.guardianproject.keanuapp.nearby.Payload
import pro.dbro.airshare.session.Peer
import pro.dbro.airshare.transport.ble.BleUtil
import timber.log.Timber

class MoreFragment : Fragment(), AirShareManager.Listener {

    companion object {
        private const val AIR_SHARE_TEST = false
    }

    private val mApp
        get() = activity?.application as? ImApp

    private var mAirShareManager: AirShareManager? = null

    private lateinit var mBinding: FragmentMoreBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        super.onCreateView(inflater, container, savedInstanceState)

        mBinding = FragmentMoreBinding.inflate(inflater, container, false)

        mBinding.btnOpenGallery.setOnClickListener {
            context?.let { it.startActivity(mApp?.router?.gallery(it)) }
        }

        mBinding.btnOpenServices.setOnClickListener { openServices() }

        mBinding.btnOpenGroups.setOnClickListener {
            (activity as? MainActivity)?.createRoom()
        }

        mBinding.btnOpenStickers.setOnClickListener {
            context?.let { it.startActivity(mApp?.router?.sticker(it)) }
        }

        mBinding.btnOpenThemes.setOnClickListener { showColors() }

        if (AIR_SHARE_TEST) {
            mBinding.btnAirShareTest.setOnClickListener { airShareTest() }

            val activity = activity
            if (activity != null) {
                val alias = (activity.application as? ImApp)?.matrixSession?.myUserId
                val serviceName = getString(R.string.app_name) // FIXME: This will cause problems when app_name gets translated.

                Timber.d(String.format("Starting AirShareManager with alias=%s, serviceName=%s", alias, serviceName))

                mAirShareManager = AirShareManager.getInstance(activity, alias, serviceName)
                        .addListener(this)
                        .startListening()
            }
        }
        else {
            mBinding.btnAirShareTest.visibility = View.GONE
        }

        return mBinding.root
    }

    override fun onResume() {
        super.onResume()

        if (AIR_SHARE_TEST && context != null && !BleUtil.isBluetoothEnabled(context)) {
            context?.startActivity(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE))
        }
    }

    override fun onDestroy() {
        AirShareManager.destroyInstance()

        super.onDestroy()
    }

    private fun showColors() {
        val context = context ?: return

        val prefs = PreferenceManager.getDefaultSharedPreferences(context)

        ColorPickerDialogBuilder
                .with(context)
                .setTitle("Choose color")
                .initialColor(prefs.getInt("themeColor", -1))
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .lightnessSliderOnly()
                .setOnColorSelectedListener { }
                .setPositiveButton(getString(R.string.ok)) { _: DialogInterface?, selectedColor: Int, _: Array<Int?>? ->
                    prefs.edit().putInt("themeColor", selectedColor).apply()

                    (activity as? MainActivity)?.applyStyle()
                }
                .setNegativeButton(getString(R.string.cancel), null)
                .build()
                .show()
    }

    private fun openServices() {
        context?.let { it.startActivity(mApp?.router?.services(it)) }
    }

    private fun airShareTest() {
        Timber.d("#airShareTest start")

        val context = context ?: return
        if (ContextCompat.checkSelfPermission(context, permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mRequestPermission.launch(permission.ACCESS_COARSE_LOCATION)
            return
        }

//        Payload payload = new Payload();
//        payload.testData = new HugeTestData(context);
//
//        mAirShareManager.send(payload);
        mAirShareManager?.sendInvite("#example_room_alias:neo.keanu.im")
    }

    private val mRequestPermission = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
        Timber.d("#requestPermission result=%b", it)

        if (it) {
            airShareTest()
        }
        else {
            AlertDialog.Builder(context)
                    .setTitle("AirShare")
                    .setMessage("Need coarse location permissions to send AirShare messages!")
                    .setNeutralButton(android.R.string.ok, null)
                    .create()
                    .show()
        }
    }

    override fun messageReceived(sender: Peer, payload: Payload) {
        val context = context ?: return

        AlertDialog.Builder(context)
                .setTitle(String.format("Message from %s", sender.alias))
                .setMessage(Gson().toJson(payload))
                .setNeutralButton(android.R.string.ok, null)
                .create()
                .show()
    }
}