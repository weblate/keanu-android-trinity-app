package info.guardianproject.keanu.core;

public interface KeanuConstants {


    String LOG_TAG = "KeanuApp";

    String EXTRA_INTENT_SEND_TO_USER = "Send2_U";

    String IMPS_CATEGORY = "org.awesomeapp.info.guardianproject.keanuapp.service.IMPS_CATEGORY";
    String ACTION_QUIT = "org.awesomeapp.info.guardianproject.keanuapp.service.QUIT";

    int SMALL_AVATAR_WIDTH = 64;
    int SMALL_AVATAR_HEIGHT = 64;

    int DEFAULT_AVATAR_WIDTH = 256;
    int DEFAULT_AVATAR_HEIGHT = 256;

    String DEFAULT_TIMEOUT_CACHEWORD = "-1"; //one day

    String CACHEWORD_PASSWORD_KEY = "pkey";
    String CLEAR_PASSWORD_KEY = "clear_key";

    String NO_CREATE_KEY = "nocreate";

    String PREFERENCE_KEY_ENCODED_PASS = "encodedpass";


    //ACCOUNT SETTINGS Imps defaults
    String DEFAULT_DEVICE_NAME = "device-android";

    String NOTIFICATION_CHANNEL_ID_SERVICE = "info.guardianproject.keanu.service";
    String NOTIFICATION_CHANNEL_ID_MESSAGE = "info.guardianproject.keanu.message.2";

    /**
     * This is to store a flag related to status like ACTIVE, MUTED, ARCHIVED
     */
    String CHAT_TYPE = "chat_type";

    int CHAT_TYPE_ACTIVE = -1;
    int CHAT_TYPE_MUTED = 1;
    int CHAT_TYPE_ARCHIVED = 2;

    int CONTACT_TYPE_FLAG_HIDDEN = 1;

    /** Message type definition */
    public interface MessageType {
        /* sent message */
        int OUTGOING = 0;
        /* received message */
        int INCOMING = 1;
        /* presence became available */
        int PRESENCE_AVAILABLE = 2;
        /* presence became away */
        int PRESENCE_AWAY = 3;
        /* presence became DND (busy) */
        int PRESENCE_DND = 4;
        /* presence became unavailable */
        int PRESENCE_UNAVAILABLE = 5;
        /* the message is converted to a group chat */
        int CONVERT_TO_GROUPCHAT = 6;
        /* generic status */
        int STATUS = 7;
        /* the message cannot be sent now, but will be sent later */
        int QUEUED = 8;

        /* the message cannot be sent now, but will be sent later */
        int SENDING = 9;

        /* received message */
        int INCOMING_ENCRYPTED = 13;

        /* received message */
        int OUTGOING_ENCRYPTED = 15;
    }

    public interface Contacts {
        /** Contact type <P>Type: INTEGER</P> */
        String TYPE = "type";

        /** normal IM contact */
        int TYPE_NORMAL = 0;
        /**
         * temporary contact, someone not in the list of contacts that we
         * subscribe presence for. Usually created because of the user is having
         * a chat session with this contact.
         */
        //  int TYPE_TEMPORARY = 1;

        /** contact created for group chat. */
        int TYPE_GROUP = 2;

        /** blocked contact. */
        int TYPE_BLOCKED = 3;

        /**
         * the contact is pinned. The client should always display this contact
         * to the user.
         */
        int TYPE_PINNED = 5;

        int TYPE_MASK = 0xff;
        int TYPE_FLAG_HIDDEN = 0x100;
        int TYPE_FLAG_UNSEEN = 0x200;

    }

}
