package info.guardianproject.keanu.core.ui.room

import android.Manifest.permission.*
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.media.AudioManager
import android.media.MediaRecorder
import android.net.Uri
import android.os.*
import android.view.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.MainActivity
import info.guardianproject.keanu.core.util.AttachmentHelper
import info.guardianproject.keanu.core.util.extensions.uris
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityRoomBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanuapp.ui.widgets.ShareRequest
import timber.log.Timber
import java.io.File
import java.util.*

open class RoomActivity : BaseActivity() {

    companion object {
        const val EXTRA_ROOM_ID = "room-id"
        const val EXTRA_SHOW_ROOM_INFO = "show-room-info"

        const val REQUEST_ADD_MEDIA = 666
    }

    var roomId: String = ""
        private set

    private var mMediaRecorder: MediaRecorder? = null
    private var mAudioFilePath: File? = null
    var isAudioRecording = false

    private val mApp: ImApp?
        get() = application as? ImApp

    var backButtonHandler: View.OnClickListener? = null

    private lateinit var mBinding: ActivityRoomBinding

    open val root: ViewGroup
        get() = mBinding.root

    open val toolbar
        get() = mBinding.toolbar

    open val mainContent: ViewGroup
        get() = mBinding.mainContent

    open val inputLayout: ViewGroup
        get() = mBinding.inputLayout

    open val historyView
        get() = mBinding.history

    open val btJumpToBottom
        get() = mBinding.btJumpToBottom

    open val composeMessage
        get() = mBinding.composeMessage

    open val btnSend
        get() = mBinding.btnSend

    open val btnMic
        get() = mBinding.btnMic

    open val btnDeleteVoice
        get() = mBinding.btnDeleteVoice

    open val btnAttach
        get() = mBinding.btnAttach

    open val attachPanel
        get() = mBinding.attachPanel

    open val btnAttachSticker
        get() = mBinding.btnAttachSticker

    open val btnAttachPicture
        get() = mBinding.btnAttachPicture

    open val btnTakePicture
        get() = mBinding.btnTakePicture

    open val btnAttachAudio
        get() = mBinding.btnAttachAudio

    open val btnAttachFile
        get() = mBinding.btnAttachFile

    open val btnCreateStory
        get() = mBinding.btnCreateStory

    open val audioRecordView
        get() = mBinding.recordView

    open val typingView
        get() = mBinding.tvTyping

    open val stickerBox
        get() = mBinding.stickerBox

    open val stickerPager
        get() = mBinding.stickerPager

    open val mediaPreviewCancel
        get() = mBinding.mediaPreviewCancel

    open val joinGroupView
        get() = mBinding.joinGroupView

    open val btnJoinAccept
        get() = mBinding.btnJoinAccept

    open val btnJoinDecline
        get() = mBinding.btnJoinDecline

    open val roomJoinTitle
        get() = mBinding.roomJoinTitle

    open val mediaPreviewContainer
        get() = mBinding.mediaPreviewContainer

    open val mediaPreview
        get() = mBinding.mediaPreview

    protected open val roomView: RoomView by lazy {
        RoomView(this, roomId)
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        mBinding = ActivityRoomBinding.inflate(layoutInflater)
        setContentView(root)

        if (intent != null) {
            roomId = intent.getStringExtra(EXTRA_ROOM_ID) ?: ""

            if (roomId.isNotEmpty()) {
                if (intent.getBooleanExtra(EXTRA_SHOW_ROOM_INFO, false)) {
                    roomView.showRoomInfo()
                    intent.putExtra(EXTRA_SHOW_ROOM_INFO, false)
                }

                mApp?.currentForegroundRoom = roomId
            }

            // Send shared items into the room, if any.
            if (intent.action == Intent.ACTION_SEND) {

                if (intent.hasExtra(Intent.EXTRA_TEXT)) {
                    var sharedMessage = intent.getStringExtra(Intent.EXTRA_TEXT)

                    if (intent.hasExtra(Intent.EXTRA_SUBJECT))
                        sharedMessage += " " + intent.getStringExtra(Intent.EXTRA_SUBJECT)

                    if (sharedMessage != null)
                        roomView.sendMessage(sharedMessage)

                }

                intent.uris.forEach {
                    if (it != null)
                        sendAttachment(it, intent.type)
                }

                // Remove attached data, to not send it again on rotate.
                intent.action = null
                intent.data = null
                intent.clipData = null

            }
        }

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        applyStyleForToolbar()

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        roomView.setSelected(true)
    }

    override fun applyStyleForToolbar() {
        supportActionBar?.title = roomView.title

        // Not set color.
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val themeColorHeader = prefs.getInt("themeColor", -1)
        var themeColorText = prefs.getInt("themeColorText", -1)
        val themeColorBg = prefs.getInt("themeColorBg", -1)

        if (themeColorHeader != -1) {
            if (themeColorText == -1) themeColorText = MainActivity.getContrastColor(themeColorHeader)

            window.navigationBarColor = themeColorHeader
            window.statusBarColor = themeColorHeader
            window.setTitleColor(themeColorText)

            toolbar.setBackgroundColor(themeColorHeader)
            toolbar.setTitleTextColor(themeColorText)
        }

        if (themeColorBg != -1) {
            mainContent.setBackgroundColor(themeColorBg)
            inputLayout.setBackgroundColor(themeColorBg)

            if (themeColorText != -1) {
                composeMessage.setTextColor(themeColorText)
                composeMessage.setHintTextColor(themeColorText)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        roomView.setSelected(true)
        mApp?.currentForegroundRoom = roomId
    }

    override fun onPause() {
        super.onPause()

        roomView.setSelected(false)
        mApp?.currentForegroundRoom = ""
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        setIntent(intent)
        // Set last read date now!
    }

    override fun onBackPressed() {
        if (backButtonHandler != null) {
            backButtonHandler?.onClick(null)
            return
        }

        super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_conversation_detail_group, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (backButtonHandler != null) {
                    backButtonHandler?.onClick(null)
                    return true
                }

                finish()
                return true
            }

            R.id.menu_end_conversation -> {
                roomView.closeChatSession()
                finish()
                return true
            }

            R.id.menu_group_info -> {
                roomView.showRoomInfo()
                return true
            }

            R.id.menu_live_mode -> {
                startLiveMode()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_ADD_MEDIA) {
            if (resultCode != RESULT_OK) return

            for (mediaUri in Matisse.obtainResult(data)) {
                sendAttachment(mediaUri)
            }
        }
        else {
            // Need to keep this for the Matisse library, which didn't get an overhaul in the
            // last 1.5 years. Grml.
            @Suppress("DEPRECATION")
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun startImagePicker() {
        if (ActivityCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE))
            {
                Snackbar.make(historyView, R.string.grant_perms, Snackbar.LENGTH_LONG)
                        .setAction(R.string.ok) {
                            startImagePicker()
                        }
                        .show()
            }
            else {
                mStartImagePickerIfPermitted.launch(READ_EXTERNAL_STORAGE)
            }
        }
        else {
            Matisse.from(this)
                    .choose(MimeType.ofAll())
                    .countable(true)
                    .maxSelectable(100)
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .thumbnailScale(0.85f)
                    .imageEngine(GlideEngine())
                    .showPreview(false)
                    .forResult(REQUEST_ADD_MEDIA)
        }
    }

    fun startPhotoTaker() {
        if (ActivityCompat.checkSelfPermission(this, CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, CAMERA))
            {
                Snackbar.make(historyView, R.string.grant_perms, Snackbar.LENGTH_LONG)
                        .setAction(R.string.ok) {
                            startPhotoTaker()
                        }
                        .show()
            }
            else {
                mStartPhotoTakerIfPermitted.launch(CAMERA)
            }
        }
        else {
            mTakePicture.launch(mApp?.router?.camera(this, oneAndDone = true))
        }
    }

    fun startFilePicker(mimeType: String = "*/*") {
        if (ActivityCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE))
            {
                Snackbar.make(historyView, R.string.grant_perms, Snackbar.LENGTH_LONG)
                        .setAction(R.string.ok) {
                            startFilePicker(mimeType)
                        }
                        .show()
            }
            else {
                mStartFilePickerIfPermitted.launch(CAMERA)
            }
        }
        else {
            // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
            // browser.
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)

            // Filter to only show results that can be "opened", such as a
            // file (as opposed to a list of contacts or timezones)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            // Filter to show only images, using the image MIME data type.
            // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
            // To search for all documents available via installed storage providers,
            // it would be "*/*".
            intent.type = mimeType

            mSendFile.launch(Intent.createChooser(intent, getString(R.string.invite_share)))
        }
    }

    fun startStoryEditor() {
        mCreateStory.launch(mApp?.router?.storyEditor(this))
    }

    fun startAudioRecording() {
        if (ActivityCompat.checkSelfPermission(this, RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, RECORD_AUDIO))
            {
                Snackbar.make(historyView, R.string.grant_perms, Snackbar.LENGTH_LONG)
                        .setAction(R.string.ok) {
                            startAudioRecording()
                        }
                        .show()
            }
            else {
                mStartAudioRecordingIfPermitted.launch(RECORD_AUDIO)
            }
        }
        else {
            val am = getSystemService(AUDIO_SERVICE) as? AudioManager

            if (am?.mode == AudioManager.MODE_NORMAL) {
                val fileName = UUID.randomUUID().toString().substring(0, 8) + ".m4a"
                mAudioFilePath = File(filesDir, fileName)

                mMediaRecorder = MediaRecorder()
                mMediaRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
                mMediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                mMediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

                // Maybe we can modify these in the future, or allow people to tweak them.
                mMediaRecorder?.setAudioChannels(1)
                mMediaRecorder?.setAudioEncodingBitRate(22050)
                mMediaRecorder?.setAudioSamplingRate(64000)
                mMediaRecorder?.setOutputFile(mAudioFilePath!!.absolutePath)

                try {
                    isAudioRecording = true
                    mMediaRecorder?.prepare()
                    mMediaRecorder?.start()
                } catch (e: Exception) {
                    Timber.e(e, "couldn't start audio")
                }
            }
        }
    }

    fun stopAudioRecording(send: Boolean) {
        if (mMediaRecorder != null && mAudioFilePath != null && isAudioRecording) {
            try {
                mMediaRecorder?.stop()
                mMediaRecorder?.reset()
                mMediaRecorder?.release()

                if (send) {
                    val uriAudio = Uri.fromFile(mAudioFilePath)
                    sendAttachment(uriAudio, "audio/mp4")
                } else {
                    mAudioFilePath?.delete()
                }
            } catch (ise: IllegalStateException) {
                Timber.w(ise, "error stopping audio recording")
            } catch (re: RuntimeException) {
                // Stop can fail so we should catch this here.

                Timber.w(re, "error stopping audio recording")
            }

            isAudioRecording = false
        }
    }

    fun sendAttachment(contentUri: Uri, fallbackMimeType: String? = null) {
        try {
            val attachment = AttachmentHelper.createFromContentUri(contentResolver, contentUri, fallbackMimeType)
            mApp?.matrixSession?.getRoom(roomId)?.sendMedia(
                    attachment,
                    true, setOf(roomId))

            roomView.jumpToBottom()
        }
        catch (e: Exception) {
            Timber.e(e, "error sending file")
        }
    }

    fun sendLink(contentUri: Uri) {
        try {
            roomView.sendMessage(contentUri.toString())
        }
        catch (e: Exception) {
            Timber.e(e, "error sending file")
        }
    }

    fun sendShareRequest(request: ShareRequest) {
        sendAttachment(request.media, request.mimeType)
    }

    private fun startLiveMode() {
        startActivity(mApp?.router?.story(this, roomId, contributorMode = true))
    }

    private val mSendFile =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult

            val uri = it.data?.data ?: return@registerForActivityResult

            sendAttachment(uri, it.data?.type)
        }

    private val mTakePicture =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult

            val request = ShareRequest()
            request.deleteFile = false
            request.resizeImage = false
            request.importContent = false
            request.media = it.data?.data
            request.mimeType = it.data?.type

            if (request.mimeType == "image/jpeg") {
                try {
                    roomView.setMediaDraft(request)
                } catch (e: Exception) {
                    Timber.w(e, "error setting media draft")
                }
            } else {
                sendShareRequest(request)
            }
        }

    private val mCreateStory =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult

            val request = ShareRequest()
            request.deleteFile = false
            request.resizeImage = false
            request.importContent = false
            request.media = it.data?.data
            request.mimeType = it.data?.type

            sendShareRequest(request)
        }

    val requestImageView = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult

            if (it.data?.hasExtra("resendImageUri") == true) {
                val request = ShareRequest()
                request.deleteFile = false
                request.resizeImage = false
                request.importContent = false
                request.media = Uri.parse(it.data?.getStringExtra("resendImageUri"))
                request.mimeType = it.data?.getStringExtra("resendImageMimeType")

                sendShareRequest(request)
            }
        }

    private val mStartImagePickerIfPermitted = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
        if (it) startImagePicker()
    }

    private val mStartPhotoTakerIfPermitted = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
        if (it) startPhotoTaker()
    }

    private val mStartAudioRecordingIfPermitted = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
        if (it) startAudioRecording()
    }

    private val mStartFilePickerIfPermitted = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
        if (it) startFilePicker()
    }
}
