package info.guardianproject.keanu.core.ui.chatlist

import android.graphics.*
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.extensions.displayNameWorkaround
import info.guardianproject.keanu.core.util.extensions.getSummary
import info.guardianproject.keanu.core.util.extensions.isArchived
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ChatListItemBinding
import org.matrix.android.sdk.api.session.content.ContentUrlResolver.ThumbnailMethod
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import java.lang.ref.WeakReference
import java.util.*
import java.util.regex.Pattern

class ChatListAdapter(listener: Listener) : RecyclerView.Adapter<ChatListItemHolder>(), Observer<List<RoomSummary>> {

    interface Listener {
        val activity: AppCompatActivity?

        fun updateListVisibility()

        fun onSelected(roomId: String)
    }

    private val mListener = WeakReference(listener)

    private val mActivity
        get() = mListener.get()?.activity

    private val mSession
        get() = (mActivity?.application as? ImApp)?.matrixSession

    private val mTypedValue = TypedValue()
    private val mBackground: Int
    private var mRoomList = listOf<RoomSummary>()
    private var mFilterString: String? = null

    private var mShowLastMessage: Boolean = true

    private val queryParams by lazy {
        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN, Membership.INVITE)
        builder.build()
    }

    init {
        mActivity?.theme?.resolveAttribute(R.attr.chatBackground, mTypedValue, true)
        mBackground = mTypedValue.resourceId
        setHasStableIds(true)

        onChanged(mSession?.getRoomSummaries(queryParams))

        mActivity?.let { mSession?.getRoomSummariesLive(queryParams)?.observe(it, this) }
    }

    fun setShowChatMessages (showLastMessage: Boolean) {
        mShowLastMessage = showLastMessage
    }

    fun filter(filterString: String?) {
        mFilterString = filterString

        onChanged(mSession?.getRoomSummaries(queryParams))
    }

    var showArchived = false
        set(value) {
            field = value

            onChanged(mSession?.getRoomSummaries(queryParams))
        }

    fun get(id: Long) : RoomSummary? {
        return mRoomList.getOrNull(id.toInt())
    }

    override fun onChanged(roomSummaries: List<RoomSummary>?) {

        var newRoomList = roomSummaries?.filter { showArchived == it.isArchived } ?: mutableListOf()

        if (mFilterString?.isNotEmpty() == true) {
            val search = Pattern.compile(Pattern.quote(mFilterString!!), Pattern.CASE_INSENSITIVE)

            newRoomList = newRoomList.filter { search.matcher(it.displayName).find() }
        }

        mRoomList = newRoomList.sortedWith(
                compareByDescending<RoomSummary> { it.membership == Membership.INVITE }
                        .then(compareByDescending {
                            it.latestPreviewableEvent?.root?.originServerTs ?: it.encryptionEventTs
                        }))

        notifyDataSetChanged()

        mListener.get()?.updateListVisibility()
    }

    fun refresh() {
        onChanged(mSession?.getRoomSummaries(queryParams))
    }

    //implement actual stable id map for room summaries
    var mRoomStableId = HashMap<String,Long>()

    override fun getItemId(position: Int): Long {
        var roomSummary = mRoomList[position]
        var roomId = mRoomStableId.get(roomSummary.roomId)
        if (roomId != null)
            return roomId
        else {
            roomId = Date().time
            roomId?.let { mRoomStableId.put(roomSummary.roomId, it) }
            return roomId
        }
    }

    override fun getItemCount(): Int {
        return mRoomList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatListItemHolder {
        val binding = ChatListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        binding.root.setBackgroundResource(mBackground)

        var viewHolder = binding.root.tag as? ChatListItemHolder

        if (viewHolder == null) {
            viewHolder = ChatListItemHolder(binding)
            binding.root.tag = viewHolder
        }

        return viewHolder
    }

    override fun onBindViewHolder(viewHolder: ChatListItemHolder, position: Int) {
        val clItem = viewHolder.itemView as? ChatListItem
        val roomSummary = mRoomList[position]
        val roomId = roomSummary.roomId
        val nickname = roomSummary.displayNameWorkaround
        var lastMsg = roomSummary.topic
        var lastMsgDate = Date().time
        val lastMsgType: String? = null
        val avatarUrl = mSession?.contentUrlResolver()?.resolveThumbnail(roomSummary.avatarUrl, 120, 120, ThumbnailMethod.SCALE)

        viewHolder.itemView

        if (roomSummary.membership == Membership.INVITE) {
            mActivity?.getString(R.string.invitation_prompt, nickname)?.let { lastMsg = it }
        }
        else {
            val tlEvent = roomSummary.latestPreviewableEvent

            if (tlEvent != null) {
                lastMsg = tlEvent.getSummary(viewHolder.itemView.context)

                tlEvent.root.originServerTs?.let { lastMsgDate = it }
            }
        }

        clItem?.bind(viewHolder, roomId, nickname, avatarUrl, lastMsg, lastMsgDate, lastMsgType, mShowLastMessage)

        if (roomSummary.hasNewMessages) {
            val ssb = SpannableStringBuilder(viewHolder.line1.text)

            ssb.setSpan(StyleSpan(Typeface.BOLD), 0, ssb.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
            ssb.setSpan(ForegroundColorSpan(Color.BLACK), 0, ssb.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)

            viewHolder.line1.text = ssb

            if (viewHolder.markerUnread.visibility == View.GONE)
                viewHolder.markerUnread.visibility = View.VISIBLE
        }
        else {
            if (viewHolder.markerUnread.visibility == View.VISIBLE)
                viewHolder.markerUnread.visibility = View.GONE
        }

        clItem?.setOnClickListener {
            mListener.get()?.onSelected(roomId)
        }
    }

    class ArchiveTouchHelperCallback(private var mOnSwiped: (position: Int) -> Unit) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT)
    {

        private val mBackground = Paint()

        private lateinit var mRecyclerView: RecyclerView

        private val mUnarchiveIcon by lazy {
            BitmapFactory.decodeResource(mRecyclerView.context.resources,
                    R.drawable.ic_unarchive_white_24dp)
        }

        private val mArchiveIcon by lazy {
            BitmapFactory.decodeResource(mRecyclerView.context.resources,
                    R.drawable.ic_archive_white_24dp)
        }

        private val mGreen by lazy {
            ResourcesCompat.getColor(mRecyclerView.context.resources, R.color.holo_green_dark, null)
        }

        private val mGray by lazy {
            Color.argb(255, 150, 150, 150)
        }

        override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                                 dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean)
        {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

            val iv = viewHolder.itemView

            if (dX > 0) {
                mRecyclerView = recyclerView

                val icon: Bitmap

                if ((recyclerView.adapter as ChatListAdapter).showArchived) {
                    icon = mUnarchiveIcon
                    mBackground.color = mGreen
                }
                else {
                    icon = mArchiveIcon
                    mBackground.color = mGray
                }

                // Draw rectangle with varying right side, equal to displacement dX.
                c.drawRect(iv.left.toFloat(), iv.top.toFloat(), dX, iv.bottom.toFloat(), mBackground)

                // Set the image icon for right swipe.
                c.drawBitmap(icon,
                        iv.left.toFloat() + (16 * (recyclerView.context.resources.displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)),
                        iv.top.toFloat() + (iv.bottom.toFloat() - iv.top.toFloat() - icon.height) / 2,
                        mBackground)


                // Fade out the view as it is swiped out of the parent's bounds.
                iv.alpha = ALPHA_FULL - kotlin.math.abs(dX) / viewHolder.itemView.width.toFloat()
                iv.translationX = dX
            }
            else {
                iv.alpha = ALPHA_FULL
                iv.translationX = 0F
            }
        }

        override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
            super.clearView(recyclerView, viewHolder)

            viewHolder.itemView.alpha = ALPHA_FULL
            viewHolder.itemView.translationX = 0F
        }

        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            mOnSwiped(viewHolder.bindingAdapterPosition)
        }
    }

    companion object {
        const val ALPHA_FULL = 1.0f
    }
}