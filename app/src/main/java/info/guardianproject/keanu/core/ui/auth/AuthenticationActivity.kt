package info.guardianproject.keanu.core.ui.auth

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.CompoundButton
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.model.Server
import info.guardianproject.keanu.core.ui.onboarding.OnboardingAccount
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanu.core.util.extensions.betterMessage
import info.guardianproject.keanu.core.util.extensions.extractUserName
import info.guardianproject.keanu.core.verification.VerificationManager
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityAuthenticationBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.MatrixPatterns
import org.matrix.android.sdk.api.auth.AuthenticationService
import org.matrix.android.sdk.api.auth.data.HomeServerConnectionConfig
import org.matrix.android.sdk.api.auth.data.LoginFlowResult
import org.matrix.android.sdk.api.auth.registration.RegistrationResult
import org.matrix.android.sdk.api.auth.registration.RegistrationWizard
import org.matrix.android.sdk.api.auth.registration.Stage
import timber.log.Timber
import java.net.MalformedURLException
import java.net.URL
import java.util.*

class AuthenticationActivity : BaseActivity(), View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    companion object {
        const val EXTRA_IS_SIGN_UP = "extra_is_sign_up"
        const val EXTRA_ONBOARDING_ACCOUNT = "extra_onboarding_account"
        const val EXTRA_USER_ID = "extra_user_id"
        const val EXTRA_PASSWORD = "extra_password"
    }

    private var mIsSignUp = false

    private lateinit var mBinding: ActivityAuthenticationBinding

    private var mHomeServer: URL? = null

    private val mApp: ImApp?
        get() = super.getApplication() as? ImApp

    private val mAuthService: AuthenticationService?
        get() = mApp?.matrix?.authenticationService()

    private val mRegistrationWizard: RegistrationWizard?
        get() = mApp?.matrix?.authenticationService()?.getRegistrationWizard()

    private val mDeviceName: String
        get() {
            val id = applicationInfo.labelRes

            val name = if (id == 0) applicationInfo.nonLocalizedLabel.toString() else getString(id)

            return if (name.isEmpty()) "Keanu3" else name
        }

    private var mOnboardingAccount: OnboardingAccount? = null

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mBinding.root))
    }

    private val mAcceptTerms = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == RESULT_OK) {
            mCoroutineScope.launch {
                try {
                    val registration = mRegistrationWizard?.acceptTerms()
                            ?: throw RuntimeException(getString(R.string.general_error, -667))

                    lifecycleScope.launch { onSuccess(registration) }
                }
                catch (failure: Throwable) {
                    lifecycleScope.launch { showError(failure) }
                }
            }
        }
        else {
            // User abort.
            mOnboardingAccount = null

            mBinding.btSignIn.isEnabled = true
            mBinding.progress.visibility = View.GONE
        }
    }

    private val mResolveCaptcha = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        val response = it.data?.getStringExtra(RecaptchaActivity.EXTRA_RESPONSE)

        if (it.resultCode == RESULT_OK && response != null) {
            mCoroutineScope.launch {
                try {
                val registration = mRegistrationWizard?.performReCaptcha(response)
                        ?: throw RuntimeException(getString(R.string.general_error, -667))

                    lifecycleScope.launch { onSuccess(registration) }
                }
                catch (failure: Throwable) {
                    lifecycleScope.launch { showError(failure) }
                }
            }
        }
        else {
            // User abort.
            mOnboardingAccount = null

            mBinding.btSignIn.isEnabled = true
            mBinding.progress.visibility = View.GONE
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityAuthenticationBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mIsSignUp = intent.getBooleanExtra(EXTRA_IS_SIGN_UP, false)

        if (!mIsSignUp) {
            mBinding.tvUsernameHint.visibility = View.GONE
            (mBinding.etConfirm.parent as? View)?.visibility = View.GONE
            mBinding.tvPasswordHint.visibility = View.GONE
        }

        mBinding.tvError.visibility = View.GONE

        val userId = intent.getStringExtra(EXTRA_USER_ID)
        mBinding.etUserName.setText(if (MatrixPatterns.isUserId(userId)) MatrixPatterns.extractUserName(userId) else userId ?: "")

        val password = intent.getStringExtra(EXTRA_PASSWORD)
        mBinding.etPassword.setText(password ?: "")

        mBinding.swAdvancedOptions.setOnCheckedChangeListener(this)
        mBinding.swCustomIdServer.setOnCheckedChangeListener(this)
        mBinding.swKeyBackup.setOnCheckedChangeListener(this)
        mBinding.swChoosePassword.setOnCheckedChangeListener(this)

        val host = MatrixPatterns.extractServerNameFromId(userId) ?: Server.getServers(this).firstOrNull()?.domain

        if (host != null) {
            mHomeServer = URL("https", host, "")

            mBinding.etHomeServer.setText(mHomeServer.toString())
            mBinding.etIdServer.setText(mHomeServer.toString())
        }

        mBinding.swKeyBackup.isChecked = true

        showAdvancedOptions(false)
        showCustomServer(false)
        showChoosePassword(true)
        showBackupPassword(false)

        mBinding.btSignIn.setOnClickListener(this)
        mBinding.btSignIn.text = getString(if (mIsSignUp) R.string.Sign_Up else R.string.sign_in)

        mBinding.progress.visibility = View.GONE

        supportActionBar?.title = getString(if (mIsSignUp) R.string.Sign_Up else R.string.sign_in)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Handle upgrade to Keanu v3 using Matrix SDK2 instead of old homegrown base:
        // If a username and password is provided, automatically try to log in.
        if (MatrixPatterns.isUserId(userId) && password?.isNotEmpty() == true) {
            mBinding.btSignIn.performClick()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        val username = mBinding.etUserName.text?.toString()
        val password = mBinding.etPassword.text?.toString()
        var homeServer = mHomeServer
        var idServer = mHomeServer
        val backupPassword: String?


        if (username.isNullOrEmpty()) {
            mBinding.etUserName.error = getString(R.string.This_field_may_not_be_empty_)

            return
        }

        mBinding.etUserName.error = null

        if (password.isNullOrEmpty()) {
            mBinding.etPassword.error = getString(R.string.This_field_may_not_be_empty_)

            return
        }

        mBinding.etPassword.error = null

        if (mIsSignUp) {
            if (password.length < 8) {
                mBinding.etPassword.error = getString(R.string.Password_is_too_short_)

                return
            }

            if (mBinding.etConfirm.text.toString() != password) {
                mBinding.etPassword.error = getString(R.string.Fields_are_not_equal_)
                mBinding.etConfirm.error = getString(R.string.Fields_are_not_equal_)

                return
            }

            mBinding.etPassword.error = null
            mBinding.etConfirm.error = null
        }

        if (mBinding.swAdvancedOptions.isChecked) {
            try {
                homeServer = URL(mBinding.etHomeServer.text.toString())

                if (homeServer.host.isNullOrEmpty()) throw MalformedURLException()
            }
            catch (e: Exception) {
                mBinding.etHomeServer.error = getString(R.string.Malformed_URL_)

                return
            }

            mBinding.etHomeServer.error = null

            if (mBinding.swCustomIdServer.isChecked) {
                try {
                    idServer = URL(mBinding.etIdServer.text.toString())

                    if (idServer.host.isNullOrEmpty()) throw MalformedURLException()
                }
                catch (e: Exception) {
                    mBinding.etIdServer.error = getString(R.string.Malformed_URL_)

                    return
                }

                mBinding.etIdServer.error = null
            }

            if (mBinding.swKeyBackup.isChecked) {
                if (mBinding.swChoosePassword.isChecked) {
                    backupPassword = mBinding.etKeyBackupPassword.text?.toString()

                    if (backupPassword.isNullOrEmpty()) {
                        mBinding.etKeyBackupPassword.error = getString(R.string.This_field_may_not_be_empty_)

                        return
                    }

                    if (mIsSignUp && backupPassword.length < 8) {
                        mBinding.etKeyBackupPassword.error = getString(R.string.Password_is_too_short_)

                        return
                    }

                    if (mBinding.etKeyBackupConfirm.text.toString() != backupPassword) {
                        mBinding.etKeyBackupPassword.error = getString(R.string.Fields_are_not_equal_)
                        mBinding.etKeyBackupConfirm.error = getString(R.string.Fields_are_not_equal_)

                        return
                    }

                    mBinding.etKeyBackupPassword.error = null
                    mBinding.etKeyBackupConfirm.error = null
                }
            }
        }

        if (homeServer == null) return

        mBinding.btSignIn.isEnabled = false
        mBinding.progress.visibility = View.VISIBLE

        if (mIsSignUp) {
            register(username, password, homeServer.toString(), idServer?.toString())
        }
        else {
            login(username, password, homeServer.toString(), idServer?.toString())
        }
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        when (buttonView) {
            mBinding.swAdvancedOptions -> showAdvancedOptions(isChecked)

            mBinding.swCustomIdServer -> showCustomServer(isChecked)

            mBinding.swKeyBackup -> showChoosePassword(isChecked)

            mBinding.swChoosePassword -> showBackupPassword(isChecked)
        }
    }

    private fun showAdvancedOptions(toggle: Boolean) {
        mBinding.llAdvancedItems.visibility = if (toggle) View.VISIBLE else View.GONE
    }

    private fun showCustomServer(toggle: Boolean) {
        mBinding.etIdServer.visibility = if (toggle) View.VISIBLE else View.GONE
    }

    private fun showChoosePassword(toggle: Boolean) {
        mBinding.swChoosePassword.visibility = if (toggle) View.VISIBLE else View.GONE

        if (!toggle) {
            mBinding.swChoosePassword.isChecked = false
        }
    }

    private fun showBackupPassword(toggle: Boolean) {
        mBinding.llKeyBackupPassword.visibility = if (toggle) View.VISIBLE else View.GONE
    }

    private fun showError(throwable: Throwable) {
        Timber.e(throwable)

        mBinding.tvError.text = throwable.betterMessage
        mBinding.tvError.visibility = View.VISIBLE

        mBinding.btSignIn.isEnabled = true
        mBinding.progress.visibility = View.GONE
    }

    /**
     * Registration requests callback.
     */
    fun onSuccess(data: RegistrationResult) {

        if (data is RegistrationResult.Success) {
            mApp?.matrixSession = data.session

            var password = mOnboardingAccount?.password

            if (password != null) {
                // Opportunistically try to enable cross signing, if not done, yet.
                // Fail silently, if not possible here.
                // User will be asked to enable cross-signing on device verification, otherwise.
                VerificationManager.instance.enableCrossSigning(password)
            }

            if (mBinding.swKeyBackup.isChecked) {
                if (!mBinding.etKeyBackupPassword.text.isNullOrEmpty()) {
                    password = mBinding.etKeyBackupPassword.text.toString()
                }

                if (password != null) {
                    // Opportunistically try to restore the latest backup or create a new key backup,
                    // if not enabled, yet.
                    // The password is either a user-provided password which is an
                    // explicit password for the key backup, or, if none provided,
                    // the user account password.
                    // Fail silently, if not possible here.
                    // User needs to enable key backup in DevicesActivity, otherwise,
                    // or will automatically be connected to key backup, when doing
                    // an interactive verification with a device which already has it.
                    VerificationManager.instance.restoreOrAddNewBackup(this, password)
                }
            }

            val intent = Intent()
            intent.putExtra(EXTRA_ONBOARDING_ACCOUNT, mOnboardingAccount)
            setResult(RESULT_OK, intent)

            finish()
            return
        }

        if (data is RegistrationResult.FlowResponse) {
            val mandatory = data.flowResult.missingStages.filter { it.mandatory }
            val stage = mandatory.firstOrNull()

            // Not success, but no mandatory stages left to fulfill?
            // Time to finally create the account!
            if (stage == null
                // Workaround: Servers with only a mandatory dummy stage want the username/password set
                // on that dummy call. The SDK can't do that, so we create the account first,
                // and let the dummy stage get fulfilled in the next round. That works.
                || (stage is Stage.Dummy && mandatory.size == 1 && mRegistrationWizard?.isRegistrationStarted == false)
            ) {
                val username = mOnboardingAccount?.username ?: return
                val password = mOnboardingAccount?.password ?: return

                mCoroutineScope.launch {
                    try {
                        val registration = mRegistrationWizard?.createAccount(username, password, mDeviceName)
                                ?: throw RuntimeException(getString(R.string.general_error, -667))

                        lifecycleScope.launch { onSuccess(registration) }
                    }
                    catch (failure: Throwable) {
                        lifecycleScope.launch { showError(failure) }
                    }
                }

                return
            }

            when (stage) {
                is Stage.ReCaptcha -> {
                    mResolveCaptcha.launch(mApp?.router?.recaptcha(this, stage.publicKey, mOnboardingAccount?.server))
                }

                is Stage.Email -> {
                    // We're not currently supporting this, yet.
                    showError(Throwable(getString(R.string.internal_server_error)))
                }
                is Stage.Msisdn -> {
                    // We're not currently supporting this, yet.
                    showError(Throwable(getString(R.string.internal_server_error)))
                }

                is Stage.Dummy -> {
                    mCoroutineScope.launch {
                        try {
                            val registration = mRegistrationWizard?.dummy()
                                ?: throw RuntimeException(getString(R.string.general_error, -667))

                            lifecycleScope.launch { onSuccess(registration) }
                        }
                        catch (failure: Throwable) {
                            lifecycleScope.launch { showError(failure) }
                        }
                    }
                }

                is Stage.Terms -> {
                    mAcceptTerms.launch(mApp?.router?.terms(this,
                        TermsActivityArgument(stage.policies.toLocalizedLoginTerms(Locale.getDefault().language))))
                }

                is Stage.Other -> {
                    // No idea, what we have to do here. Must be a future server with
                    // additional requirements this SDK version cannot fulfill.
                    showError(Throwable(getString(R.string.internal_server_error)))
                }
            }
        }
    }

    /**
     * Registration requests callback.
     */
    fun onFailure(failure: Throwable) {
        showError(failure)
    }

    private suspend fun getLoginFlow(homeServer: String, idServer: String?): LoginFlowResult {
        var hServer = Uri.parse(homeServer)

        if (hServer.scheme?.toLowerCase(Locale.ROOT)?.startsWith("http") != true) {
            hServer = hServer.buildUpon().scheme("https").build()
        }

        val builder = HomeServerConnectionConfig.Builder()

        builder.withHomeServerUri(hServer)

        if (idServer != null) {
            var iServer = Uri.parse(idServer)

            if (iServer.scheme?.toLowerCase(Locale.ROOT)?.startsWith("http") != true) {
                iServer = iServer.buildUpon().scheme("https").build()
            }

            builder.withIdentityServerUri(iServer)
        }

        val loginFlow = try {
            mAuthService?.getLoginFlow(builder.build())
        }
        catch (failure: Throwable) {
            null
        }

        if (loginFlow is LoginFlowResult) {
            if (!loginFlow.isLoginAndRegistrationSupported) {
                throw RuntimeException(getString(R.string.not_implemented))
            }

            return loginFlow
        }
        else {
            throw RuntimeException(getString(R.string.general_error, -666))
        }
    }

    private fun register(username: String, password: String, homeServer: String, idServer: String?)
    {
        mCoroutineScope.launch {
            mApp?.signOut()

            try {
                val loginFlow = getLoginFlow(homeServer, idServer)

                val registration = mRegistrationWizard?.getRegistrationFlow()
                        ?: throw RuntimeException(getString(R.string.general_error, -667))

                mOnboardingAccount = OnboardingAccount(username, loginFlow.homeServerUrl, password, true)

                lifecycleScope.launch { onSuccess(registration) }
            }
            catch (failure: Throwable) {
                lifecycleScope.launch { showError(failure) }
            }
        }
    }

    private fun login(username: String, password: String, homeServer: String, idServer: String?)
    {
        mCoroutineScope.launch {
            mApp?.signOut()

            try {
                val loginFlow = getLoginFlow(homeServer, idServer)

                val session = mAuthService?.getLoginWizard()?.login(username, password, mDeviceName)
                        ?: throw RuntimeException(getString(R.string.general_error, -668))

                mOnboardingAccount = OnboardingAccount(username, loginFlow.homeServerUrl, password, false)

                lifecycleScope.launch { onSuccess(RegistrationResult.Success(session)) }
            }
            catch (failure: Throwable) {
                lifecycleScope.launch { showError(failure) }
            }
        }
    }
}