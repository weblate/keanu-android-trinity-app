package info.guardianproject.keanu.core.ui.friends

import android.app.Activity
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FriendViewBinding
import org.matrix.android.sdk.api.session.content.ContentUrlResolver.ThumbnailMethod
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.user.model.User
import java.util.*

class FriendsRecyclerViewAdapter(private val mFragment: FriendsListFragment) : RecyclerView.Adapter<FriendViewHolder>() {

    private val mTypedValue = TypedValue()

    private val mBackground: Int

    private val mContext: Activity?

    private var mFriendList = ArrayList<User?>()

    private val mApp
        get() = mContext?.application as? ImApp

    private val mSession
        get() = mApp?.matrixSession


    init {
        mContext = mFragment.activity

        mContext?.theme?.resolveAttribute(R.attr.chatBackground, mTypedValue, true)

        mBackground = mTypedValue.resourceId

        setHasStableIds(true)

        val builder = RoomSummaryQueryParams.Builder()
        updateFriends(mSession?.getRoomSummaries(builder.build()))
        val list = mSession?.getRoomSummariesLive(builder.build())
        list?.observe(mFragment, { roomSummaries: List<RoomSummary> -> updateFriends(roomSummaries) })
    }

    private fun updateFriends(roomSummaries: List<RoomSummary>?) {
        val userMap = HashMap<String, User?>()

        if (roomSummaries != null) {
            for (summary in roomSummaries) {
                val membersCount = summary.joinedMembersCount

                if ((summary.isDirect || membersCount == null || membersCount <= 2) && summary.otherMemberIds.isNotEmpty()) {
                    userMap[summary.otherMemberIds[0]] = mSession?.getUser(summary.otherMemberIds[0])
                }
            }
        }

        mFriendList = ArrayList(userMap.values)

        notifyDataSetChanged()

        mFragment.updateListVisibility()
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return mFriendList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FriendViewHolder {
        val binding = FriendViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        binding.root.setBackgroundResource(mBackground)

        var viewHolder = binding.root.tag as? FriendViewHolder
        if (viewHolder == null) {
            viewHolder = FriendViewHolder(binding.root)
            binding.root.tag = viewHolder
        }

        return viewHolder
    }

    override fun onBindViewHolder(viewHolder: FriendViewHolder, position: Int) {
        val user = mFriendList[position]
        val nickname = user?.displayName
        val address = user?.userId
        var avatarUrl = user?.avatarUrl

        avatarUrl = mSession?.contentUrlResolver()?.resolveThumbnail(avatarUrl, 120, 120, ThumbnailMethod.SCALE)

        val item = viewHolder.itemView as? FriendListItem
        item?.bind(viewHolder, address, nickname, avatarUrl)
        item?.setOnClickListener { v: View ->
            val context = v.context
            context.startActivity(mApp?.router?.friend(context, address))
        }
    }
}