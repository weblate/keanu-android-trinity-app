    package info.guardianproject.keanu.core.ui.room

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Matrix
import android.net.Uri
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.github.barteksc.pdfviewer.PDFView
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.StyledPlayerView
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.ui.widgets.MessageViewHolder
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.StoryViewerExoPlayerBinding
import info.guardianproject.keanuapp.databinding.StoryViewerFileInfoBinding
import info.guardianproject.keanuapp.ui.widgets.*
import info.guardianproject.keanuapp.ui.widgets.AudioRecorder.AudioRecorderListener
import info.guardianproject.keanuapp.ui.widgets.PZSImageView.PSZImageViewImageMatrixListener
import timber.log.Timber
import java.io.File
import java.io.FileInputStream

/**
 * Created by N-Pex on 2019-03-28.
 */
@SuppressLint("ClickableViewAccessibility")
open class StoryView(activity: StoryActivity, roomId: String) : RoomView(activity, roomId), AudioRecorderListener {

    companion object {
        private const val AUTO_ADVANCE_TIMEOUT_IMAGE = 5000 // Milliseconds
        private const val AUTO_ADVANCE_TIMEOUT_PDF = 5000 // Milliseconds
    }

    private val mStoryActivity
        get() = mActivity as StoryActivity

    private val mSnapHelper = PagerSnapHelper()

    private val mStoryAudioPlayer = StoryAudioPlayer(activity)

    private var mAudioRecorder: AudioRecorder? = null

    private var mCurrentPage = -1

    private var mCurrentPageViewHolder: RecyclerView.ViewHolder? = null

    /**
     * Set to true to automatically advance to next media item. For images this is after a set time, for video and audio when they are played.
     */
    private var mAutoAdvance = true

    /**
     * Set to true if we get an auto advance event while on the last item.
     */
    private var mWaitingForMoreData = false

    /**
     * If this is set, we are in "preview audio" mode.
     */
    private var mRecordedAudio: MediaInfo? = null

    private val mAdvanceToNextRunnable = Runnable { advanceToNext() }


    init {
        val llm = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        llm.stackFromEnd = false
        mStoryActivity.historyView.layoutManager = llm

        mSnapHelper.attachToRecyclerView(mStoryActivity.historyView)

        mStoryActivity.historyView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (mCurrentPage != currentPagePosition) {
                        // Only react on change.
                        setCurrentPage()
                        updateProgressCurrentPage()
                        mAutoAdvance = true
                    }
                }
                else if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    mStoryActivity.historyView.removeCallbacks(mAdvanceToNextRunnable)
                    mAutoAdvance = false
                }
            }
        })

        mStoryActivity.btnMic.setOnClickListener(null)
        mStoryActivity.btnMic.setOnLongClickListener {
            captureAudioStart()
            true
        }
        mStoryActivity.btnMic.setOnTouchListener { _, event ->
            if (mAudioRecorder?.isAudioRecording == true && (event.actionMasked == MotionEvent.ACTION_UP || event.actionMasked == MotionEvent.ACTION_CANCEL)) {
                captureAudioStop() // Stop!
            }
            false
        }

        mStoryActivity.previewAudio.visibility = View.GONE

        mStoryActivity.storyAudioPlayerView.showShuffleButton = false
        mStoryActivity.storyAudioPlayerView.setShowMultiWindowTimeBar(true)
        mStoryActivity.storyAudioPlayerView.hide()
        mStoryActivity.storyAudioPlayerView.player = mStoryAudioPlayer.player

        val fabShowAudioPlayer = mStoryActivity.fabShowAudioPlayer

        fabShowAudioPlayer.setOnClickListener {
            val lp = fabShowAudioPlayer.layoutParams as CoordinatorLayout.LayoutParams

            if (mStoryActivity.storyAudioPlayerView.visibility == View.VISIBLE) {
                mStoryActivity.storyAudioPlayerView.hide()
                lp.dodgeInsetEdges = Gravity.BOTTOM
                fabShowAudioPlayer.setImageResource(R.drawable.ic_audio_24dp)
            }
            else {
                mStoryActivity.storyAudioPlayerView.show()
                lp.dodgeInsetEdges = 0
                fabShowAudioPlayer.setImageResource(R.drawable.ic_close_white_24dp)
                mStoryActivity.storyAudioPlayerView.player
            }

            fabShowAudioPlayer.layoutParams = lp
        }

        mStoryActivity.composeMessage.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                mAutoAdvance = false
            }

            override fun afterTextChanged(s: Editable) {}
        })

        mStoryActivity.composeMessage.clearFocus()
    }

    override fun onSendButtonClicked() {
        // If we have recorded audio, send that!
        if (mRecordedAudio != null) {
            mStoryActivity.sendMedia(mRecordedAudio?.uri, "audio/m4a")
            setRecordedAudio(null)

            return
        }

        super.onSendButtonClicked()
    }

    override fun sendMessage() {
        super.sendMessage()

        val view = mActivity.currentFocus
        if (view != null) {
            (mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)
                    ?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private val currentPagePosition: Int
        get() {
            val lm = mStoryActivity.historyView.layoutManager
            val snapView = mSnapHelper.findSnapView(lm)

            if (lm != null && snapView != null) return lm.getPosition(snapView)

            return RecyclerView.NO_POSITION
        }

    private fun updateProgressCurrentPage() {
        if (mCurrentPage >= 0) {
            mStoryActivity.progressBar.progress = mCurrentPage + 1
        }
    }

    private fun setCurrentPage() {
        if (mCurrentPageViewHolder != null) {
            (mCurrentPageViewHolder?.itemView as? StyledPlayerView)?.player?.playWhenReady = false
        }

        mCurrentPage = currentPagePosition

        mCurrentPageViewHolder = if (mCurrentPage >= 0) mStoryActivity.historyView.findViewHolderForAdapterPosition(mCurrentPage) else null

        if (mCurrentPageViewHolder != null) {
            (mCurrentPageViewHolder?.itemView as? StyledPlayerView)?.player?.playWhenReady = true

            if (mCurrentPageViewHolder?.itemView is PZSImageView) {
                mStoryActivity.historyView.removeCallbacks(mAdvanceToNextRunnable)
                mStoryActivity.historyView.postDelayed(mAdvanceToNextRunnable, AUTO_ADVANCE_TIMEOUT_IMAGE.toLong())
            }
            else if (mCurrentPageViewHolder?.itemView is PDFView) {
                mStoryActivity.historyView.removeCallbacks(mAdvanceToNextRunnable)
                mStoryActivity.historyView.postDelayed(mAdvanceToNextRunnable, AUTO_ADVANCE_TIMEOUT_PDF.toLong())
            }
        }
    }

    override fun createRecyclerViewAdapter(): MessageListAdapter? {
        return StoryRecyclerViewAdapter(mStoryActivity, mRoomId)
    }

    private fun captureAudioStart() {
        mStoryActivity.composeMessage.visibility = View.INVISIBLE

        // Start recording!
        if (mAudioRecorder == null) {
            mAudioRecorder = AudioRecorder(mStoryActivity.previewAudio.context, this)
        }
        else if (mAudioRecorder?.isAudioRecording == true) {
            mAudioRecorder?.stopAudioRecording(true)
        }

        StoryExoPlayerManager.recordAudio(mAudioRecorder, mStoryActivity.previewAudio)
        mAudioRecorder?.startAudioRecording()

        mStoryActivity.btnMic.isAnimating = true
    }

    private fun captureAudioStop() {
        mStoryActivity.btnMic.isAnimating = false

        if (mAudioRecorder?.isAudioRecording == true) {
            mAudioRecorder?.stopAudioRecording(false)
        }
    }

    private fun setRecordedAudio(recordedAudio: MediaInfo?) {
        mRecordedAudio = recordedAudio

        if (mRecordedAudio != null) {
            mStoryActivity.btnMic.visibility = View.GONE
            mStoryActivity.btnSend.visibility = View.VISIBLE

            val d = ActivityCompat.getDrawable(mActivity, R.drawable.ic_close_white_24dp)?.mutate()
            if (d != null) DrawableCompat.setTint(d, Color.GRAY)

            mActivity.supportActionBar?.setHomeAsUpIndicator(d)

            mActivity.backButtonHandler = View.OnClickListener {
                StoryExoPlayerManager.stop(mStoryActivity.previewAudio)
                setRecordedAudio(null)
            }
        }
        else {
            mActivity.supportActionBar?.setHomeAsUpIndicator(null)
            mActivity.backButtonHandler = null

            mStoryActivity.previewAudio.visibility = View.GONE

            mStoryActivity.composeMessage.visibility = View.VISIBLE
            mStoryActivity.composeMessage.setText("")

            mStoryActivity.btnMic.visibility = View.VISIBLE
        }
    }

    override fun onAudioRecorded(uri: Uri) {
        setRecordedAudio(MediaInfo(uri, "audio/mp4"))

        StoryExoPlayerManager.load(mRecordedAudio, mStoryActivity.previewAudio, true)
    }

    internal inner class StoryRecyclerViewAdapter(context: AppCompatActivity, roomId: String) : MessageListAdapter(context, this@StoryView, roomId), PSZImageViewImageMatrixListener {

        private val imageRequestOptions = RequestOptions().centerInside().diskCacheStrategy(DiskCacheStrategy.NONE).error(R.drawable.broken_image_large)

        override fun getItemCount(): Int {
            val count = 100 //set Item count here

            mStoryActivity.progressBar.max = count
            updateProgressCurrentPage()

            return count
        }

        override fun getItemViewType(position: Int): Int {
            val mime = "image/jpeg" //set mime type here

            if (mime.isNotEmpty()) {
                if (mime.startsWith("audio/") || mime.startsWith("video/")) {
                    return 1
                }
                else if (mime == "application/pdf") {
                    return 2
                }
            }

            return 0 // Image
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
            val mediaView: View
            val context = parent.context

            when (viewType) {
                2 -> mediaView = StoryViewerFileInfoBinding.inflate(LayoutInflater.from(context), parent, false).root

                1 -> {
                    mediaView = StoryViewerExoPlayerBinding.inflate(LayoutInflater.from(context), parent, false).root
                    mediaView.setBackgroundColor(-0xcccccd)
                }

                0 -> {
                    mediaView = PZSImageView(context)
                    mediaView.setBackgroundColor(-0xcccccd)
                }

                else -> {
                    mediaView = PZSImageView(context)
                    mediaView.setBackgroundColor(-0xcccccd)
                }
            }

            val lp = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            mediaView.layoutParams = lp

            val mvh = MessageViewHolder(mediaView)
            mvh.onImageClickedListener = this

            return mvh
        }

        //TODO fix data lookup
        @SuppressLint("CheckResult")
        override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
            val viewType = getItemViewType(position)
            val context = viewHolder.itemView.context

            try {
                val mime = "image/jpeg"
                val uri = Uri.parse("file://someimage.jpg")

                when (viewType) {
                    2 -> {
                        (viewHolder.itemView.findViewById(R.id.text) as? TextView)?.text = uri.lastPathSegment

                        viewHolder.itemView.setOnClickListener {
                            context.startActivity(ImApp.sImApp?.router?.pdfView(context, uri, mime))
                        }
                    }

                    1 -> {
                        val playerView = viewHolder.itemView as? StyledPlayerView
                        val mediaInfo = MediaInfo(uri, mime)

                        StoryExoPlayerManager.load(mediaInfo, playerView, false)

                        playerView?.player?.addListener(object : Player.Listener {

                            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                                if (playbackState == Player.STATE_ENDED) {
                                    advanceToNext()
                                }
                            }
                        })
                    }

                    else -> {
                        val imageView = viewHolder.itemView as? PZSImageView ?: return

                        try {
                            imageView.setMatrixListener(this)

                            val glide = Glide.with(context)
                                    .asBitmap()
                                    .apply(imageRequestOptions)

                            if (SecureMediaStore.isVfsUri(uri)) {
                                val path = uri.path
                                val fileMedia = if (path != null) File(path) else null

                                if (fileMedia?.exists() == true) {
                                    glide.load(FileInputStream(fileMedia))
                                }
                                else {
                                    glide.load(R.drawable.broken_image_large)
                                }
                            }
                            else {
                                glide.load(uri)
                            }

                            glide.into(imageView)
                        }
                        catch (t: Throwable) { // May run out of memory.
                            Timber.w("unable to load thumbnail: $t")
                        }
                    }
                }
            }
            catch (e: Exception) {
                Timber.d(e)
            }

            if (mCurrentPage == -1) {
                mCurrentPage = 0
                mStoryActivity.historyView.post { setCurrentPage() }
            }
        }

        override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
            val pdfView = holder.itemView as? PDFView

            if (pdfView != null) {
                pdfView.post { pdfView.recycle() }

                return
            }

            val playerView = holder.itemView as? StyledPlayerView

            if (playerView != null) {
                playerView.player?.stop()
                playerView.player?.clearMediaItems()
                playerView.player?.release()
                playerView.player = null
            }

            super.onViewRecycled(holder)
        }

        override fun onImageMatrixSet(view: PZSImageView, imageWidth: Int, imageHeight: Int, imageMatrix: Matrix) {
            //TODO
        }
    }

    private fun advanceToNext() {
        if (!mAutoAdvance || mCurrentPage < 0) return

        // At end of data?
        if (mCurrentPage + 1 < mStoryActivity.historyView.adapter?.itemCount ?: 0) {
            mWaitingForMoreData = false
            mStoryActivity.historyView.smoothScrollToPosition(mCurrentPage + 1)
        }
        else {
            mWaitingForMoreData = true
        }
    }

    fun pause() {
        mStoryAudioPlayer.player.stop()
    }
}