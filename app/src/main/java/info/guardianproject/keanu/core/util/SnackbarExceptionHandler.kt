package info.guardianproject.keanu.core.util

import android.view.View
import com.google.android.material.snackbar.Snackbar
import info.guardianproject.keanu.core.util.extensions.betterMessage
import kotlinx.coroutines.CoroutineExceptionHandler
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

/**
 * Base exception handler which shows a more-or-less useful error message as a Snackbar,
 * when the root coroutine cancels because of an exception.
 *
 * You might use it like this:
 *
 * ```Kotlin
 * private val mCoroutineScope: CoroutineScope by lazy {
 *     CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mBinding.root))
 * }
 * ```
 */
class SnackbarExceptionHandler(private val view: View?) : CoroutineExceptionHandler {

    override val key: CoroutineContext.Key<*>
        get() = CoroutineExceptionHandler

    override fun handleException(context: CoroutineContext, exception: Throwable) {
        Timber.d(exception)

        view?.let { Snackbar.make(it, exception.betterMessage, Snackbar.LENGTH_LONG).show() }
    }
}