package info.guardianproject.keanu.core.ui.onboarding

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.provider.Telephony
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanuapp.R
import timber.log.Timber

class OnboardingManager {

    data class DecodedInviteLink(
            @JvmField
            var username: String? = null,

            @JvmField
            var fingerprint: String? = null,

            @JvmField
            var nickname: String? = null
    )

    companion object {
        const val REQUEST_SCAN = 1111

        private const val BASE_INVITE_URL = "https://keanu.im/i/#"

        private const val DEFAULT_SCHEME = "matrix"

        @JvmStatic
        fun inviteSMSContact(context: Activity, message: String?) {
            val defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(context) // Need to change the build to API 19
            val sendIntent = Intent(Intent.ACTION_SEND)
            sendIntent.type = "text/plain"
            sendIntent.putExtra(Intent.EXTRA_TEXT, message)

            // Can be null in case that there is no default, then the user would be able to choose
            // any app that support this intent.
            if (defaultSmsPackageName != null) sendIntent.setPackage(defaultSmsPackageName)

            context.startActivity(sendIntent)
        }

        @JvmStatic
        fun inviteShare(context: Activity, message: String?) {
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_TEXT, message)
            intent.type = "text/plain"

            context.startActivity(intent)
        }

        @JvmStatic
        fun inviteNearby(context: Activity, message: String?) {
            context.startActivity((context.application as? ImApp)?.router?.nearbyAddContact(context, message, "text/plain"))
        }

        @JvmStatic
        fun inviteShareToPackage(context: Activity, message: String?, packageName: String?) {
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_TEXT, message)
            intent.type = "text/plain"
            intent.setPackage(packageName)

            context.startActivity(intent)
        }

        @JvmStatic
        fun inviteScan(context: Activity, message: String?) {
            context.startActivityForResult((context.application as? ImApp)?.router
                ?.qrScan(context, message, "text/plain"), REQUEST_SCAN)
        }

        @JvmStatic
        fun generateInviteMessage(context: Context, nickname: String, username: String): String? {
            return try {
                (nickname + ' ' + context.getString(R.string.is_inviting_you) + " "
                        + generateInviteLink(username))
            }
            catch (e: Exception) {
                Timber.d(e, "Error with link.")
                null
            }
        }

        @JvmStatic
        fun decodeInviteLink(link: String): DecodedInviteLink? {
            if (link.contains("/i/#")) {
                // This is an invite link like this: https://keanu.im/i/#@foobar:matrix.org

                if (link.contains("@")) {
                    try {
                        val matrixContact = link.substring(link.lastIndexOf("@"))
                        return DecodedInviteLink(matrixContact)
                    }
                    catch (iae: IllegalArgumentException) {
                        Timber.e(iae, "bad link decode")
                    }
                }

                if (link.contains("!")) {
                    try {
                        val matrixContact = link.substring(link.lastIndexOf("!"))
                        return DecodedInviteLink(matrixContact)
                    }
                    catch (iae: IllegalArgumentException) {
                        Timber.e(iae, "bad link decode")
                    }
                }
            }
            else if (link.contains("matrix.to")) {
                // This is an invite link like this: https://matrix.to/#/@foobar:matrix.org

                try {
                    val matrixContact = link.substring(link.lastIndexOf("@"))
                    return DecodedInviteLink(matrixContact)
                }
                catch (iae: IllegalArgumentException) {
                    Timber.e(iae, "bad link decode")
                }
            }
            else if (link.startsWith(DEFAULT_SCHEME)) {
                // This is an invite link like this: https://matrix.to/#/@foobar:matrix.org

                try {
                    val matrixContact = link.substring(link.lastIndexOf("id=") + 3)
                    return DecodedInviteLink(matrixContact)
                }
                catch (iae: IllegalArgumentException) {
                    Timber.e(iae, "bad link decode")
                }
            }

            return null
        }

        @JvmStatic
        fun generateMatrixLink(username: String): String {
            return "$DEFAULT_SCHEME://invite?id=$username"
        }

        fun generateInviteLink(username: String): String {
            return BASE_INVITE_URL + username
        }
    }
}