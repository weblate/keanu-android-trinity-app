package info.guardianproject.keanu.core.ui.friends

import android.app.Activity
import android.database.DataSetObserver
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListAdapter
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanuapp.databinding.FriendViewBinding
import org.matrix.android.sdk.api.session.content.ContentUrlResolver.ThumbnailMethod
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.user.model.User
import java.util.*

class FriendsListAdapter(private val mContext: Activity, filter: String?) : ListAdapter {

    private val mSession
        get() = (mContext.application as? ImApp)?.matrixSession

    private var mFriendsList = ArrayList<User>()

    private var mListRoomSummaries: List<RoomSummary>? = null

    private var mFilter: String? = null


    init {
        mFilter = filter
        initData()
    }


    fun setFilter(filter: String?) {
        mFilter = filter
        initData()
    }

    private fun initData() {
        if (mListRoomSummaries == null) {
            val builder = RoomSummaryQueryParams.Builder()
            mListRoomSummaries = mSession?.getRoomSummaries(builder.build())
        }

        val userMap = HashMap<String, User?>()

        for ((_, displayName, _, _, _, _, _, _, isDirect, _, joinedMembersCount, _, _, otherMemberIds) in mListRoomSummaries!!) {
            if (isDirect || joinedMembersCount!! <= 2 && otherMemberIds.isNotEmpty()) {
                if (otherMemberIds.isNotEmpty()) {
                    if (mFilter?.isNotEmpty() == true) {
                        if (!displayName.contains(mFilter!!)) continue
                    }

                    userMap[otherMemberIds[0]] = mSession?.getUser(otherMemberIds[0])
                }
            }
        }

        mFriendsList = ArrayList(userMap.values)
    }

    override fun areAllItemsEnabled(): Boolean {
        return true
    }

    override fun isEnabled(position: Int): Boolean {
        return true
    }

    override fun registerDataSetObserver(observer: DataSetObserver) {}

    override fun unregisterDataSetObserver(observer: DataSetObserver) {}

    override fun getCount(): Int {
        return mFriendsList.size
    }

    override fun getItem(position: Int): User {
        return mFriendsList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        val binding = FriendViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        var viewHolder = binding.root.tag as? FriendViewHolder

        if (viewHolder == null) {
            viewHolder = FriendViewHolder(binding.root)
            binding.root.tag = viewHolder
        }

        var (address, nickname, avatarUrl) = mFriendsList[position]
        avatarUrl = mSession?.contentUrlResolver()?.resolveThumbnail(avatarUrl, 120, 120, ThumbnailMethod.SCALE)

        val item = viewHolder.itemView as? FriendListItem
        item?.bind(viewHolder, address, nickname, avatarUrl)

        return binding.root
    }

    override fun getItemViewType(position: Int): Int {
        return 1
    }

    override fun getViewTypeCount(): Int {
        return 1
    }

    override fun isEmpty(): Boolean {
        return mFriendsList.isEmpty()
    }
}