package info.guardianproject.keanu.core.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.content.ContextCompat;

import java.util.Objects;

import info.guardianproject.keanu.core.ImApp;
import info.guardianproject.keanu.core.util.Debug;

public class GeneralJobIntentService extends JobIntentService {

    public static final int JOB_ID = 0x01;

    private boolean isBooted = false;

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, GeneralJobIntentService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {

        Context context = getApplicationContext();

        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction()))
            handleBoot(context,intent);
    }

    private void handleBoot (Context context, Intent intent)
    {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        boolean prefStartOnBoot = prefs.getBoolean("pref_start_on_boot", true);

        Debug.onServiceStart();
        if (prefStartOnBoot) {

            if (!isBooted) {
                ContextCompat.startForegroundService(context,
                        Objects.requireNonNull(ImApp.Companion.getSImApp()).getRouter().remoteService(context, true));
                
                isBooted = true;
            }
        }
    }
}
